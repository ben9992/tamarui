# TamarUI

## Production
### Pre deployment:
1) main.cpp - engine.load("qrc:/qml/main.qml");
2) UiVideoWgt.qml - visiable: true
3) Add Configuration folder and Elbit.qml.video
4) Add RTI License to main folder
5) main.qml - change x pos

### Deployment:
1) Run C:\Qt\Qt5.12.4\5.12.4\msvc2017_64\bin\windeployqt.exe --qmldir <path-to-app-qml-files> <path-to-app-binary>
2) Run Publisher :
		A) For file : VideoPublisher.exe -name:suzi -source:file -path:"C:/Users/tc06673/Downloads/Castle.mp4"
		B) For AverMedia (Publisher for AverMedia) : VideoPublisher.exe -name:suzi -deviceId:4 -resolution:44
3) Take all depends from CarmelUI

## Debug
Please insert as arguments to main() the paths to UserSetting and FactorySetting.
In Visual studio : 
1) Right click on the Project
2) Go to Properties
3) Go to Debugging
4) Insert in Command Arguments:

-f C:\Development\TamarUi\Main\Configuration\Tamar\ -u C:\ProgramData\ElbitSystemsLtd\Configuration\Tamar\