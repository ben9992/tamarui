#pragma once
#include <QObject>
#include <QMetaType>

class ProjectCommon : public QObject
{
    Q_OBJECT
public:
    enum MonitorDBIndex
    {
        // Memory Database

        FileStoreDBEnum,                            // g:Config
        CommDefsStoreDBEnum,                        // g:Config
        ParamsDefsStoreDBEnum,                      // g:Config
        //ReticlesStsDBEnum,                        //r:ReticlesStatusDBEnum        g:ReticleSystem
        //ReticlesCmdDBEnum,                        //r:ReticlesCommandDBEnum       g:ReticleSystem
        TopBarStsDBEnum,                            //r:TopBarStsDBEnum             g:BattleScreen
        TopBarCmdDBEnum,                            //r:TopBarCmdDBEnum             g:BattleScreen
        GuiCommonStsDBEnum,                         //r:GuiCommonStsDBEnum          g:GuiCommon
        GuiCommonCmdDBEnum,                         //r:GuiCommonCmdDBEnum          g:GuiCommon
        VehicleStsDBEnum,                           //r:VehicleStsDBEnum            g:Vehicle
        VehicleCmdDBEnum,                           //r:VehicleCmdDBEnum            g:Vehicle
        NumberOfDatabases
    };

    enum AlertStatusEnum
    {
        ALERT_VANISHED,
        ALERT_EXIST,
    };

    enum ErrorStatusEnum
    {
        NOT_EXIST		= 0x01,
        EXIST			= 0x02,
        PEND_TO_EXIST	= 0x04,
        VANISHED		= 0x08,
        REPEATED		= 0x10,
        PEND_TO_REPEAT	= 0x20
    };

    enum LanguageEnum
    {
        ENGLISH_LANGUAGE = 0,
        CROATIAN_LANGUAGE = 1,
        RUMAINIAN_LANGUAGE = 2
    };

    enum EnabledEnum
    {
        ENABLED_NONE 		= -1,
        ENABLED_FALSE		= 0,
        ENABLED_TRUE		= 1,
        ENABLED_FALSE_TEMP  = 2
    };



    enum FunctionStepEnum
    {
        STEP_NONE = -1,
        STEP_DOWN = 0,
        NO_STEP = 1,
        STEP_UP = 2
    };

    enum CommunicationEnum
    {
        COMMUNICATION_NONE		= -1,
        COMMUNICATION_FAILURE	= 0,
        COMMUNICATION_OK		= 1
    };

    enum BoolEnum
    {
        BOOL_NONE			= -1,
        BOOL_FALSE			= 0,
        BOOL_TRUE			= 1
    };

    enum StatusEnum
    {
        STATUS_NONE			= -1,
        STATUS_FAIL			= 0,
        STATUS_OK			= 1
    };

    enum SwitchEnum
    {
        SWITCH_NONE = -1,
        SWITCH_OFF  = 0,
        SWITCH_ON   = 1
    };

    enum ProcessEnum
    {
        PROCESS_ERROR				= -2,
        PROCESS_NONE 				= -1,
        PROCESS_STOPPED				= 0,
        PROCESS_RUNNING				= 1,
        PROCESS_FINISHED_OK			= 2,
        PROCESS_FINISHED_FAIL		= 3
    };


    enum ProcessCmdEnum
    {
        PROCESS_CMD_NONE = -1,
        PROCESS_CMD_ENTER = 0,
        PROCESS_CMD_EXIT = 1,
        PROCESS_CMD_ABORT = 2,
        PROCESS_CMD_RESET = 3
    };

    enum BitEnum
    {
        BIT_NONE	= -1,
        BIT_FAIL	= 0,
        BIT_OK		= 1
    };

    enum BitTypeEnum
    {
        BIT_TYPE_NONE				= -1,
        BIT_TYPE_INITIALIZATION		= 0,
        BIT_TYPE_PERIODIC			= 1,
        BIT_TYPE_INITIATED			= 2
    };

    enum OnOffStateEnum
    {
        STATE_ERROR   =-2,
        STATE_NONE    =-1,
        STATE_OFF     =0,
        STATE_ON      =1
    };

    enum BoresightModeEnum
    {
        BS_MODE_NOT_IN_PROGRESS,
        BS_MODE_IN_PROGRESS,
        BS_MODE_EXIT_SUCCESS,
        BS_MODE_EXIT_FAIL,
        BS_MODE_EXIT_FAIL_TRV,
        BS_MODE_EXIT_FAIL_ELV,
        BS_MODE_EXIT_ABORTED,
    };

    enum ReticleMovementEnum
    {
        RETICLE_MOVEMENT_NO_MOVEMENT = -1,
        RETICLE_MOVEMENT_STEP_UP,
        RETICLE_MOVEMENT_STEP_DOWN,
        RETICLE_MOVEMENT_STEP_LEFT,
        RETICLE_MOVEMENT_STEP_RIGHT
    };

    enum AngleUnitsEnum
    {
        ANGLE_UNITS_UNDEFINED = -1,
        ANGLE_UNITS_RADS,
        ANGLE_UNITS_MILLS,
        ANGLE_UNITS_MRADS,
        ANGLE_UNITS_DEGS
    };

    enum PositionUnitsEnum
    {
        POSITION_UNITS_UNDEFINED = -1,
        POSITION_UNITS_PIXELS

    };

    enum ValidityEnum
    {
        VALIDITY_NONE           = -1,
        VALIDITY_INDETERMINATE  = 0,
        VALID                   = 1,
        NOT_VALID               = 2
    };

    enum FunctionModeEnum
    {
        FUNC_MODE_NONE      = -1,
        FUNC_MODE_MANUAL    = 0,
        FUNC_MODE_AUTO      = 1
    };

    enum CalibrationTypeEnum
    {
        CALIBRATION_NONE            = -1,
        CALIBRATION_IDLE            = 0,
        CALIBRATION_GSE             = 1,
        CALIBRATION_TSE             = 2,
        CALIBRATION_GPOD_ELV_SE		= 3,
        CALIBRATION_GPOD_TRV_SE		= 4,
        CALIBRATION_LIMITS          = 5,
        CALIBRATION_ANGLE_SNS		= 6,
        CALIBRATION_GUN_FRICTION    = 7,
        CALIBRATION_GPOD_FRICTION	= 8,
        CALIBRATION_BACKLASH        = 9,
        CALIBRATION_GLRF_RETICLE	= 10,
        CALIBRATION_CLRF_RETICLE	= 11,
        CALIBRATION_MLP				= 12,
        CALIBRATION_CONFIRM         = 13,
        CALIBRATION_ABORT           = 14,
        CALIBRATION_RESET           = 15
    };

    enum WorkModeEnum
    {
        WM_NONE = -1,
        WM_MANUAL = 0,
        WM_POWER_ON = 1,
        WM_STAB = 2
    };

    enum CmdrSightEnslvModeEnum
    {
        CS_EM_FAULT = -2,
        CS_EM_NONE = -1,
        CS_EM_CTG = 0,
        CS_EM_IND = 1,
        CS_EM_INIT = 2
    };

    enum OperatorEnum
    {
        OPERATOR_NONE = -1,
        OPERATOR_GUNNER = 0,
        OPERATOR_COMMANDER = 1,

        NUM_OF_OPERATORS
    };

    enum SightTypeEnum
    {
        SIGHT_NONE = -1,
        SIGHT_GUNNER = 0,
        SIGHT_COMMANDER = 1,
        NUM_OF_SIGHTS
    };

    enum HatchesStatusEnum
    {
        HATCHES_NONE = -1,
        HATCHES_OPEN = 0,
        HATCHES_CLOSED = 1
    };

    enum WeaponTypeEnum
    {
        WEAPON_NONE = -1,
        WEAPON_GUN = 0,
        WEAPON_MAG = 1,
        WEAPON_MSL = 2,

        NUM_OF_WEAPONS
    };

    enum AmmunitionTypeEnum
    {
        AMMU_NONE = -1,
        AMMU_AP,
        AMMU_TP,
        AMMU_HE,
        AMMU_MG,
        AMMU_ABM,


        NUM_OF_AMMUNITIONS  = 25   // Current Max supported by Ballistics
    };

    enum WeaponFireModeEnum
    {
        FIRE_MODE_NONE      = -1,
        FIRE_MODE_SINGLE    = 0,
        FIRE_MODE_BURST     = 1,
        FIRE_MODE_FULL      = 2
    };

    enum WeaponFireRateEnum
    {
        FIRE_RATE_NONE = -1,
        FIRE_RATE_LOW,
        FIRE_RATE_HIGH
    };

    enum PowerEnum
    {
        POWER_NONE          = -1,
        POWER_OFF           = 0,
        POWER_ON            = 1,
        POWER_STANDBY       = 2
    };

    enum RangeTypeEnum
    {
        RANGE_TYPE_NONE 		= -1,
        RANGE_TYPE_BATTLE		= 0,
        RANGE_TYPE_MANUAL		= 1,
        RANGE_TYPE_LASER		= 2
    };

    enum RangeMeasureEnum
    {
        RANGE_MEASURE_ERROR = -2,
        RANGE_MEASURE_NONE = -1,
        RANGE_MEASURE_NO_RANGE = 0,
        RANGE_MEASURE_SINGLE = 1,
        RANGE_MEASURE_MULTIPLE = 2
    };

    enum RangeEchoEnum
    {
        RANGE_ECHO_NONE = -1,
        RANGE_ECHO_FIRST = 0,
        RANGE_ECHO_LAST = 1
    };

    enum SelectedMagazineEnum
    {
        MGZ_NONE_SEL = 0,
        MGZ1_SEL = 1,
        MGZ2_SEL = 2,
        MAG_MGZ_SEL = 3,
    };

    enum PodConfigurationEnum
    {
        POD_BASIC = 0,
        POD_1_AXIS = 1,
        POD_2_AXIS = 2
    };

    enum PodAxisEnum
    {
        POD_AXIS_ELV = 0,
        POD_AXIS_TRV = 1,
        MAX_NOF_POD_AXIS = 2
    };

    enum ABMFiringModeEnum
    {
        IMPACT = 0,
        PDD,
        PT,
        LINE_HOR,
        LINE_DEPTH,
        AREA_HOR,
        AREA_VERTICAL,

        NUM_OF_ABM_FIRING_MODES,
        NON_ABM = -1,
    };

    enum AbmScenarioTypeEnum
    {
        OPEN_SCENARIO,
        URBAN_SCENARIO,

        NUM_OF_TABLE_SCENARIOS
    };

    enum TravelLockStateEnum
    {
        TRAVEL_LOCK_NONE = -1,
        TRAVEL_LOCK_FAILURE,
        TRAVEL_LOCK_LOCKED,
        TRAVEL_LOCK_STUCK,
        TRAVEL_LOCK_UNLOCKED,
    };

    /*
    MLP_NONE = 0,	//MLP_DISABLE_CURRENT
    MLP_ELV_SET_POSITION_CMD,// todo: add location cmd as value in the relevant input to control
    MLP_IN_PROCESS,	// internal status, not used for communication with control
    MLP_FAULT		// internal status, not used for communication with control
    */
    enum MlpOperationalStateEnum
    {
        MLP_DISABLE_CURRENT = 0,
        MLP_RAISE,
        MLP_FOLD,
        ENS_MLP_TO_SIGHT,
        ENS_SIGHT_TO_MLP,
        MLP_ELV_SET_POSITION_CMD,
        MLP_MAINTENANCE,
        ENS_MLP_TO_GUN,
        MLP_FAULT
    };

    enum MlpStatusEnum
    {
        MLP_FOLDED = 0,
        MLP_MIDWAY = 1,
        MLP_RAISED = 2,
        MLP_RAISE_SE_OUT_OF_RANGE = 3,
        MLP_ELV_SE_OUT_OF_RANGE = 4,
        MLP_BOTH_SE_OUT_OF_RANGE = 5,
    };

    enum MlpCalibStateEnum
    {
        MLP_CALIB_IDLE,
        MLP_CALIB_ELV_UPPER,
        MLP_CALIB_ZERO,
        MLP_CALIB_ELV_LOWER,
        MLP_CALIB_FOLDED
    };

    enum MslMessagesTypesEnum
    {
        MSL_ALERT_LINE,
        MSL_ERROR_LINE,
        MSL_HIGH_PRIORITY_ERROR_LINE,
        ACTIVITY_TIMED_MSG,
        ACTIVITY_UNTIMED_MSG
    };

    enum NumberOfMissilesEnum
    {
        NO_MISSILE = 0,
        ONE_MISSILE = 1,
        TWO_MISSILES = 2
    };

    enum MslLoadingProcessEnum
    {
        MSL_LOAD_PROC_NONE,
        MSL_LOAD_PROC_FAILED,
        MSL_LOAD_PROC_ABORTED,
        MSL_LOAD_PROC_FINISH_SUCCESS,
        MSL_LOAD_PROC_FOLDING,
        MSL_LOAD_PROC_FOLD_E_LOCKED,
        MSL_LOAD_PROC_TRV_MOVE,
        MSL_LOAD_PROC_NOW_LOADING,
    };

    Q_ENUM(CommunicationEnum)
    Q_ENUM(RangeMeasureEnum)
    Q_ENUM(WorkModeEnum)
    Q_ENUM(WeaponTypeEnum)
    Q_ENUM(AmmunitionTypeEnum)
    Q_ENUM(ReticleMovementEnum)
    Q_ENUM(WeaponFireModeEnum)
    Q_ENUM(WeaponFireRateEnum)
    Q_ENUM(SelectedMagazineEnum)
    Q_ENUM(CalibrationTypeEnum)
    Q_ENUM(FunctionModeEnum)
    Q_ENUM(ProcessEnum)
    Q_ENUM(RangeTypeEnum)
    Q_ENUM(RangeEchoEnum)
    Q_ENUM(OnOffStateEnum)
    Q_ENUM(AngleUnitsEnum)
    Q_ENUM(SwitchEnum)
    Q_ENUM(HatchesStatusEnum)
    Q_ENUM(SightTypeEnum)
    Q_ENUM(AbmScenarioTypeEnum)
    Q_ENUM(BitEnum)
    Q_ENUM(ValidityEnum)
    Q_ENUM(BoolEnum)
    Q_ENUM(CmdrSightEnslvModeEnum)
    Q_ENUM(ABMFiringModeEnum)
    Q_ENUM(MlpCalibStateEnum)
    Q_ENUM(TravelLockStateEnum)
    Q_ENUM(OperatorEnum)
    Q_ENUM(MslLoadingProcessEnum)
    Q_ENUM(MlpStatusEnum)
    Q_ENUM(PowerEnum)
    Q_ENUM(BoresightModeEnum)
    Q_ENUM(PositionUnitsEnum)
    Q_ENUM(FunctionStepEnum)
    Q_ENUM(EnabledEnum)

#pragma pack(1)

    struct CoordinatesStruct
    {
        float	fCordX;
        float	fCordY;
    };

    struct ParamsStruct
    {
            char m_strVideoChannelName1[32];
            char m_strVideoChannelName2[32];
            char m_strVideoChannelName3[32];
            char m_strVideoChannelName4[32];
            char m_strVideoChannelName5[32];
            char m_strWebClientUrl[512];
    };


    struct DualFloatValueStruct
    {
        float   fTrv;
        float   fElv;
    };

#pragma pack()

};
