TEMPLATE = app
QT += qml webview gui quick multimedia xml

QT_DEBUG_PLUGINS=1

CONFIG += c++11
CONFIG += console
QTPLUGIN += qsvg qsvgicon

# <-- Versioning from file Version.txt -->
# set a qmake variable
TAMAR_VERSION = "$$cat(../Version.txt)"
# propagate the variable to C/C++
DEFINES += "TAMAR_VERSION=\\\"$$TAMAR_VERSION\\\""

VERSIONS = $$split(TAMAR_VERSION, ".")

VERSION_MAJOR = $$member(VERSIONS, 0)
VERSION_MINOR = $$member(VERSIONS, 1)
VERSION_PATCH = $$member(VERSIONS, 2)
VERSION_BUILD = $$member(VERSIONS, 3)

DEFINES += "VERSION_MAJOR=\\\"$$VERSION_MAJOR\\\""
DEFINES += "VERSION_MINOR=\\\"$$VERSION_MINOR\\\""
DEFINES += "VERSION_PATCH=\\\"$$VERSION_PATCH\\\""
DEFINES += "VERSION_BUILD=\\\"$$VERSION_BUILD\\\""
# <-- Versioning from file Version.txt -->

QMAKE_CXXFLAGS_RELEASE += /Zi
QMAKE_LFLAGS_RELEASE += /DEBUG
QMAKE_PROJECT_DEPTH = 0
TARGET = TamarUI

ARCH = ""
win32 {
    !contains(QT_ARCH, x86_64) {
        ARCH += "x86"
    } else {
        ARCH += "x86_64"
    }

    QMAKE_CXXFLAGS += /bigobj
}
# <-- Run App As Administrator -->
QMAKE_LFLAGS_WINDOWS += "/MANIFESTUAC:\"level='requireAdministrator' uiAccess='false'\""

BUILD_TYPE = ""

CONFIG(debug, debug|release){
    BUILD_TYPE += "Debug"
}else{
    BUILD_TYPE += "Release"
}

system(conan install . -s arch=$$ARCH -s build_type=$$BUILD_TYPE)
include(conanbuildinfo.pri)

INCLUDEPATH += $$CONAN_INCLUDEPATH_INFRASTRUCTURE
INCLUDEPATH += $$CONAN_INFRASTRUCTURE_ROOT/ModernAPI
INCLUDEPATH += $$CONAN_INFRASTRUCTURE_ROOT/Common
LIBS += $$CONAN_LIBDIRS_INFRASTRUCTURE -lezframework -lmemory_database -lports -lprotocols -ldirect_input_device -lxinput_device -lcommon_files

#INCLUDEPATH += $$CONAN_DDSADDON_ROOT/Include
#INCLUDEPATH += $$CONAN_DDSADDON_ROOT/ModernAPI
#INCLUDEPATH += $$CONAN_DDSADDON_ROOT/Common
# LIBS+= $$CONAN_LIBDIRS_DDSADDON -ldds_database

#DEFINES += RTI_WIN32
#DEFINES += NDDS_DLL_VARIABLE
#INCLUDEPATH += $$CONAN_INCLUDEPATH_DDS
#INCLUDEPATH += $$CONAN_INCLUDEPATH_DDS/ndds
#INCLUDEPATH += $$CONAN_INCLUDEPATH_DDS/ndds/hpp

INCLUDEPATH += ./../ \
Model \
Common

# CONFIG(debug, debug|release){
#     LIBS += $$CONAN_LIBDIRS_DDS -lnddscpp2d -lnddscored -lnddscd
# }else{
#     LIBS += $$CONAN_LIBDIRS_DDS -lnddscpp2 -lnddscore -lnddsc
# }

WORKSPACE_FILE_PATH = $$absolute_path(DE/workspace.de)
EZ_FRAMEWORK_PATH = "EZ_FRAMEWORK_PATH::$$CONAN_INFRASTRUCTURE_ROOT"
# DDSADDON_PATH = "DDSADDON_PATH::$$CONAN_DDSADDON_ROOT"

paths = $$EZ_FRAMEWORK_PATH  # $$DDSADDON_PATH

write_file($$WORKSPACE_FILE_PATH, paths)

RESOURCES += \
    qml.qrc

SOURCES += \
    ViewModel/QConfiguration.cpp \
    ViewModel/QConstants.cpp \
    ViewModel/QDatabaseConnector.cpp \
    ViewModel/QGuiConnector.cpp \
    ViewModel/QGuiTypes.cpp \
    ViewModel/MyTranslator.cpp \
    main.cpp

HEADERS += \
    Common/CommDefsStore.h \
    Common/FileStore.h \
    Helpers/Constants.h \
    Helpers/QQmlAutoPropertyHelpers.h \
    Helpers/QQmlHelpersCommon.h \
    Helpers/QQmlReloader.h \
    Helpers/Utils.h \
    Common/ParamStore.h \
    Common/ProjectCommon.h \
    ViewModel/MyTranslator.h \
    ViewModel/QConfiguration.h \
    ViewModel/QConfiguration.h \
    ViewModel/QConstants.h \
    ViewModel/QDatabaseConnector.h \
    ViewModel/QGuiConnector.h \
    ViewModel/QGuiTypes.h \ \
    ViewModel/MyTranslator.h \
