#pragma once
#include "QQmlAutoPropertyHelpers.h"
#include <QGuiApplication>
#include <QScreen>
#include <QPixmap>
#include <sstream>
#include <iomanip>


static std::string getTimestampInString(bool isFilePath = false)
{
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    if(isFilePath) strftime(buffer,sizeof(buffer),"%d-%m-%Y_%H-%M-%S",timeinfo);
    else strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
    std::string timestamp(buffer);
    return timestamp;
}

static std::string convertIdToStr(const long long& msb,const long long& lsb)
{
    std::stringstream msbStream;
    msbStream << std::setfill('0') << std::setw(sizeof(unsigned long long) * 2) << std::hex << static_cast<unsigned long long>(msb);
    std::string msbStr(msbStream.str());

    std::stringstream lsbStream;
    lsbStream << std::setfill('0') << std::setw(sizeof(unsigned long long) * 2) << std::hex << static_cast<unsigned long long>(lsb);
    std::string lsbStr(lsbStream.str());

    std::string idStr("{00000000-0000-0000-0000-000000000000}");
    for (UINT i = 1; i < 9; i++)   { idStr[i] = msbStr[i - 1];  }
    for (UINT i = 10; i < 14; i++) { idStr[i] = msbStr[i - 2];  }
    for (UINT i = 15; i < 19; i++) { idStr[i] = msbStr[i - 3];  }
    for (UINT i = 20; i < 24; i++) { idStr[i] = lsbStr[i - 20]; }
    for (UINT i = 25; i < 37; i++) { idStr[i] = lsbStr[i - 21]; }

    return idStr;
}

static std::string convertIdToStr(int32_t resourceId, int32_t instanceId)
{
    long long lsb = 0;
    long long msb = (static_cast<long long>(resourceId) << 32) + static_cast<long long>(instanceId);
    return convertIdToStr(msb,lsb);
}



static bool checkIdStrFormat(const char* idStr)
{
    /*
     * Id Format = {00000000-0000-0000-0000-000000000000}
     * The zeros can be replaced by any hex char: <0-9> | <a-f> | <A-F>
     */
    return (std::strlen(idStr) == 38)
            && (idStr[0] == '{')
            && (idStr[9] == '-')
            && (idStr[14] == '-')
            && (idStr[19] == '-')
            && (idStr[24] == '-')
            && (idStr[37] == '}');
}

static void convertStrToId(const char* str, long long& msb, long long& lsb)
{
    /*
     * Removing all occurrences of '-' , '{' , '}'
     * from a string of the form {00000000-0000-0000-0000-000000000000}
     * The produced string, idHexStr is a sequential representation of the id
     * that later would be converted to two std::int_64t numbers
     */

    if(checkIdStrFormat(str) == false)
        return;

    char msbStr[17] = {0};
    char lsbStr[17] = {0};

    for (int i = 1; i < 9; i++)   { msbStr[i-1] = str[i];  }
    for (int i = 10; i < 14; i++) { msbStr[i-2] = str[i];  }
    for (int i = 15; i < 19; i++) { msbStr[i-3] = str[i];  }
    for (int i = 20; i < 24; i++) { lsbStr[i-20] = str[i]; }
    for (int i = 25; i < 37; i++) { lsbStr[i-21] = str[i]; }

    msb = static_cast<long long>(std::strtoull(msbStr, nullptr, 16));
    lsb = static_cast<long long>(std::strtoull(lsbStr, nullptr, 16));
}


struct ButtonInfo
{
private:
    static constexpr size_t I_COUNT = 16;

    size_t m_i;     // button cell in button array
    uint8_t m_j;    // button bit in button cell
    bool m_lastPressedState;
    bool m_released;
    bool m_justPressed;

public:
    ButtonInfo(size_t i, uint8_t j) :
        m_i(i), m_j(j), m_lastPressedState(false), m_released(false), m_justPressed(false)
    {
        if (i >= I_COUNT)
            throw std::invalid_argument("i");
    }

    ButtonInfo() :
        m_lastPressedState(false), m_released(false), m_justPressed(false)
    {}

    virtual ~ButtonInfo()
    {}

    ButtonInfo(const ButtonInfo& other) :
        m_i(other.m_i),
        m_j(other.m_j)
    {}

    ButtonInfo& operator=(const ButtonInfo& other)
    {
        m_i = other.m_i;
        m_j = other.m_j;

        return *this;
    }

    bool pressed(const uint8_t(&buttons)[16])
    {
        return ((buttons[m_i] & (1 << m_j)) > 0);
    }

    bool pressed()
    {
        return m_lastPressedState;
    }

    bool justPressed()
    {
        //in order to use this method you need to run the update method in every buttons event
        return m_justPressed;
    }

    bool released()
    {
        //in order to use this method you need to run the update method on every buttons event
        return m_released;
    }

    void update(const uint8_t(&buttons)[16])
    {
        m_released = ((m_lastPressedState == true) && (pressed(buttons) == false));
        m_justPressed = ((m_lastPressedState == false) && (pressed(buttons) == true));
        m_lastPressedState = pressed(buttons);
    }
};

template<typename T>
static QVariantList vectorToList(const QVector<T>& vec)
{
    QVariantList list;
    list.reserve(vec.size());
    for(int i = 0 ; i < vec.size() ; i++)
        list.append(QVariant::fromValue<T>(vec.at(i)));
    return list;
}

template<typename T>
static QVector<T> listToVector(const QVariantList& list)
{
    QVector<T> vec;
    vec.reserve(list.size());
    for(int i = 0 ; i < list.size() ; i++)
        vec.append(list.at(i).value<T>());
    return vec;
}

static bool printScreen(){
    QPixmap originalPixmap; // clear image for low memory situations
    QScreen *screen = QGuiApplication::primaryScreen();
    if (screen)
    {
        originalPixmap = screen->grabWindow(0);    //<== some widget
        std::string time = getTimestampInString(true);
        QString fileName = "screenshot_" + QString(time.c_str()) + ".jpg";
        originalPixmap.save(fileName,"jpg");
        return true;
    }
    return false;
}
