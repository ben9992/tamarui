#include "ProjectCommon.h"
#include "ParamStore.h"
#include "FileStore.h"
#include "CommDefsStore.h"


#include "Helpers/Constants.h"
#include "Helpers/QQmlReloader.h"
#include "ViewModel/QGuiConnector.h"
#include "ViewModel/QGuiTypes.h"
#include "ViewModel/MyTranslator.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QPointer>
#include <QProcess>
#include <QFileInfo>
#include <QScreen>
#include <QPixmap>
#include <QImage>
#include <QtWebView/QtWebView>

#include <Factories.hpp>
#include <Monitor.hpp>
#include <XMLLoader.hpp>
#include <INILoader.hpp>
//#include <DDS/Factories.hpp>

std::function<void()> QQmlReload::m_reloadFunc;

class TamarUiBuilder : public Application::Builder
{
public:
    Application::MainApp m_mainApp;
private:
    static constexpr size_t MAX_DATA_SIZE = 2048;
    Database::DataSet m_datasetMemory;
    //Database::DataSet m_datasetDDS;
    Files::FilesHandler m_handler;
    Common::CommonTypes::ClientVariableLengthProtocolStruct m_ClientVariableLengthProtocolMemoryDBMonitor;
    //Common::CommonTypes::ClientVariableLengthProtocolStruct m_ClientVariableLengthProtocolDDSMonitor;
    ProjectCommon::LanguageEnum m_langParams;
    QPointer<TamarUi::QGuiConnector> m_connector;

protected:
    virtual void BuildEnvironment() override
    {
        InitMemoryDataset();
        InitDDSDataset();

        try
        {
            //this will Work only if factory/user path is a valid path with conf.xml file
            ReadParamsStore();
        }
        catch (...)
        {
            //use default
            Core::Console::ColorPrint(Core::Console::Colors::YELLOW, "configuration files not found, using default\n");

            throw std::runtime_error("ReadParamsStore");
        }

        std::stringstream os;
        m_datasetMemory[ProjectCommon::MonitorDBIndex::CommDefsStoreDBEnum][CommDefsStore::CommDefsStoreDBEnum::MONITORING_COMM_DEFS].Read<Common::CommonTypes::ClientVariableLengthProtocolStruct>(m_ClientVariableLengthProtocolMemoryDBMonitor);
        os << "Build Monitor remote address " << m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stRemoteIP.IPAddress << ":"
           << m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stRemoteIP.port <<
              " local address " << m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stLocalIP.IPAddress << ":" <<
              m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stLocalIP.port;
        Core::Console::ColorPrint(Core::Console::Colors::WHITE, "%s\n", os.str().c_str());

        //create the memory monitor listener
        AddRunnable<Database::Monitor>(m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stRemoteIP.IPAddress,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stRemoteIP.port,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stLocalIP.IPAddress,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stClientStruct.stLocalIP.port,
                                       m_datasetMemory,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stVariableLengthProtocol.nSizeOfDataSize,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stVariableLengthProtocol.nPlaceOfDataSize,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stVariableLengthProtocol.bAbsoluteDataSize,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stVariableLengthProtocol.nMaxDataSize,
                                       m_ClientVariableLengthProtocolMemoryDBMonitor.stVariableLengthProtocol.nConstatSizeAddition
                                       );
//        std::stringstream osDDS;
//        m_datasetMemory[ProjectCommon::MonitorDBIndex::CommDefsStoreDBEnum][CommDefsStore::CommDefsStoreDBEnum::DDS_MONITORING_COMM_DEFS].Read<Common::CommonTypes::ClientVariableLengthProtocolStruct>(m_ClientVariableLengthProtocolDDSMonitor);
//        osDDS << "Build DDS Monitor remote address " << m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stRemoteIP.IPAddress << ":"
//           << m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stRemoteIP.port <<
//              " local address " << m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stLocalIP.IPAddress << ":" <<
//              m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stLocalIP.port;
//        Core::Console::ColorPrint(Core::Console::Colors::WHITE, "%s\n", osDDS.str().c_str());

//        //create the dds monitor listener
//        AddRunnable<Database::Monitor>(m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stRemoteIP.IPAddress,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stRemoteIP.port,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stLocalIP.IPAddress,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stClientStruct.stLocalIP.port,
//                                       m_datasetDDS,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stVariableLengthProtocol.nSizeOfDataSize,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stVariableLengthProtocol.nPlaceOfDataSize,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stVariableLengthProtocol.bAbsoluteDataSize,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stVariableLengthProtocol.nMaxDataSize,
//                                       m_ClientVariableLengthProtocolDDSMonitor.stVariableLengthProtocol.nConstatSizeAddition
//                                       );
    }

    virtual void BuildDispatchers() override
    {
        m_connector = new TamarUi::QGuiConnector(m_datasetMemory);
    }

    void OnInitialized() override
    {
        m_connector->Init();
    }

    void OnStarted() override
    {
        m_connector->Start();
    }

    void ReadParamsStore()
    {
        std::string  xmlFactorySetting = "";
        std::string  xmlDeveloperSetting = "";
        std::string  xmlUserSetting = "";
        std::string  INIFilePath = "";
        m_handler = Files::FilesHandler::Create();

        xmlFactorySetting = m_mainApp.FactorySettings() + "FileStore.ini";

        xmlUserSetting = m_mainApp.UserSettings() + "FileStore.ini";
        AddRunnable<ConfigurationLoader::INIStoreDB>(ProjectCommon::MonitorDBIndex::FileStoreDBEnum,
                                                     m_datasetMemory,
                                                     xmlFactorySetting,
                                                     "",
                                                     xmlUserSetting,
                                                     m_handler,
                                                     "",
                                                     "");

        Common::CommonTypes::CHARBUFFER strCommdefs = {};
        m_datasetMemory[ProjectCommon::MonitorDBIndex::FileStoreDBEnum][FileStore::FileStoreDBEnum::COMM_DEFS_PATH].Read<Common::CommonTypes::CHARBUFFER>(strCommdefs);
        xmlFactorySetting = m_mainApp.FactorySettings() + strCommdefs.buffer;
        xmlUserSetting = m_mainApp.UserSettings() + strCommdefs.buffer;
        AddRunnable<ConfigurationLoader::XmlStoreDB>(ProjectCommon::MonitorDBIndex::CommDefsStoreDBEnum,
                                                     m_datasetMemory,
                                                     xmlFactorySetting,
                                                     "",
                                                     xmlUserSetting,
                                                     m_handler,
                                                     "",
                                                     "");

        m_datasetMemory[ProjectCommon::MonitorDBIndex::FileStoreDBEnum][FileStore::FileStoreDBEnum::PARAMS_DEFS_PATH].Read<Common::CommonTypes::CHARBUFFER>(strCommdefs);
        xmlFactorySetting = m_mainApp.FactorySettings() + strCommdefs.buffer;
        xmlUserSetting = m_mainApp.UserSettings() + strCommdefs.buffer;
        AddRunnable<ConfigurationLoader::XmlStoreDB>(ProjectCommon::MonitorDBIndex::ParamsDefsStoreDBEnum,
                                                     m_datasetMemory,
                                                     xmlFactorySetting,
                                                     "",
                                                     xmlUserSetting,
                                                     m_handler,
                                                     "",
                                                     "");

        //m_datasetMemory[ProjectCommon::MonitorDBIndex::ParamsDefsStoreDBEnum][ParamStore::LANGUAGE_PARAMS_DEFS].Read<ProjectCommon::LanguageEnum>(m_langParams);
        //MyTranslator::Instance()->updateLanguage(m_langParams);


    }

public:
    TamarUiBuilder()
    {

    }

    TamarUi::QGuiConnector* GuiConnector()
    {
        return m_connector.data();
    }


    void InitMemoryDataset()
    {
        // init memory dataset
        m_datasetMemory = Database::MemoryDatabase::Create("LocalDataSet");
        m_datasetMemory.AddTable(ProjectCommon::MonitorDBIndex::FileStoreDBEnum);
        m_datasetMemory.AddTable(ProjectCommon::MonitorDBIndex::CommDefsStoreDBEnum);
        m_datasetMemory.AddTable(ProjectCommon::MonitorDBIndex::ParamsDefsStoreDBEnum);

        //m_datasetMemory[ProjectCommon::MonitorDBIndex::VehicleStsDBEnum].AddRow<double>(VehicleDB::ELV_VEHICLE_STATUS);
    }
    void InitDDSDataset()
    {
        // init DDS dataset
        // m_datasetDDS = Database::DDSDatabase::Create(MrssJapanDDS::DatasetName());
        // m_datasetDDS.AddTable(MrssJapanDDS::MonitorDBIndex::MrssCommandDDSDBEnum);

        // m_datasetDDS[MrssJapanDDS::MonitorDBIndex::MrssCommandDDSDBEnum].AddRow<Core::Framework::VersionStruct>(MrssInterface::MrssCommandDDSDBEnum::KEEP_ALIVE_ECHO);

    }
};


int main(int argc,const char *argv[])
{

#ifdef TIMER_PROPERTIES
    PropertyUpdateTimer::Instance().Start(1000.0 / TIMER_PROPERTIES_TICKS_PER_SECOND);
#endif

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_DisableShaderDiskCache);

    QGuiApplication appGUI(argc,(char **)argv);
    QtWebView::initialize();
    MyTranslator mTrans(&appGUI);

    TamarUiBuilder builder;
    builder.m_mainApp = Application::MainApp(argc, argv);
    builder.Build();

    qmlRegisterType<QQmlReload>("qmlReload", 1,0, "QmlReload");
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QString("guiConnector"), builder.GuiConnector());

    static const char* const qmlPath = "qrc:/qml/main.qml";

    engine.load(qmlPath);
    QQmlReload::SetReloadFunc([&]()
    {
        foreach (QObject* obj, engine.rootObjects())
        {
            obj->deleteLater();
        }

        engine.loadData(QByteArray());
        engine.clearComponentCache();
        engine.load(qmlPath);
    });

    if (engine.rootObjects().isEmpty())
        return -1;

    std::stringstream osVersion;
    osVersion << "Tamar UI Version " << TAMAR_VERSION << std::endl;

    Core::Console::ColorPrint(Core::Console::Colors::WHITE, "%s\n", osVersion.str().c_str());


    return appGUI.exec();
}
