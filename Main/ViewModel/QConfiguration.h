#pragma once
#include "Helpers/QQmlAutoPropertyHelpers.h"
#include "ViewModel/QDatabaseConnector.h"
#include "ViewModel/QGuiTypes.h"
#include "ParamStore.h"
#include "ProjectCommon.h"

namespace TamarUi
{
    class QGlobalConfiguration : public QDatabaseConnector
    {
        Q_OBJECT

    public:

        QML_WRITABLE_AUTO_PROPERTY(QString, videoChannelName1)
        QML_WRITABLE_AUTO_PROPERTY(QString, videoChannelName2)
        QML_WRITABLE_AUTO_PROPERTY(QString, videoChannelName3)
        QML_WRITABLE_AUTO_PROPERTY(QString, videoChannelName4)
        QML_WRITABLE_AUTO_PROPERTY(QString, videoChannelName5)
        QML_WRITABLE_AUTO_PROPERTY(QString, webClientUrl)

    private:
        Database::DataSet m_dataset;

        virtual void Init(){}
        virtual void Start(){}
        virtual void Stop(){}


    public:
        QGlobalConfiguration(const Database::DataSet& dataset, const Utils::Context& context = nullptr, QObject* parent = nullptr);
        explicit QGlobalConfiguration(QObject* parent = nullptr);
    };
}
