﻿#pragma once
#include "ProjectCommon.h"
#include "ParamStore.h"

#include "ViewModel/QConstants.h"
#include "ViewModel/QConfiguration.h"
#include "ViewModel/QGuiTypes.h"
#include "ViewModel/QDatabaseConnector.h"

#include <QQmlEngine>

namespace TamarUi
{
    static constexpr int TYPES_VERSION_MAJOR = 1;
    static constexpr int TYPES_VERSION_MINOR = 0;

    class QGuiConnector : public QDatabaseConnector
    {
        Q_OBJECT
        QML_WRITABLE_AUTO_PROPERTY(TamarUi::QConstants*, constants)
        QML_WRITABLE_AUTO_PROPERTY(TamarUi::QGlobalConfiguration*, configuration)

    private:
        Database::DataSet m_datasetMemory;
        Database::SubscriptionsCollector m_subscriptions;

    private:
        template <typename T>
        inline void RegisterType(const char* name)
        {
            qmlRegisterType<T>("TamarUi", TYPES_VERSION_MAJOR, TYPES_VERSION_MINOR, name);
        }

        void RegisterTypes();

    private slots:


    public:
        QGuiConnector(const Database::DataSet& memoryDataset, QObject* parent = nullptr);
        QGuiConnector(QObject* parent = nullptr);
        virtual ~QGuiConnector() override;

        void Init() override;
        void Start() override;
        void Stop() override;

    };
}
