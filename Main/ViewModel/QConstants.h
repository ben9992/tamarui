#pragma once
#include "Helpers/QQmlAutoPropertyHelpers.h"

namespace TamarUi
{
class QConstants : public QObject
{
    Q_OBJECT

private:
    QML_CONSTANT_AUTO_PROPERTY(double, deg2rag)
    QML_CONSTANT_AUTO_PROPERTY(double, rad2deg)
    QML_CONSTANT_AUTO_PROPERTY(QString, MyAppVersion)

public:
    explicit QConstants(QObject* parent = nullptr);
};
}
