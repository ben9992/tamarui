#include "QConstants.h"
#include "Helpers/Constants.h"

TamarUi::QConstants::QConstants(QObject *parent) :
    QObject(parent),
    m_deg2rag(DEG2RAD),
    m_rad2deg(RAD2DEG),
    m_MyAppVersion(APP_VERSION)
{
}
