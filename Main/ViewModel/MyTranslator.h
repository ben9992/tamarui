#pragma once
#include <QObject>

#include "ParamStore.h"
#include "ProjectCommon.h"

#include <QGuiApplication>
#include <QTranslator>

class MyTranslator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString emptyString READ getEmptyString NOTIFY languageChanged)


public:
    MyTranslator(QGuiApplication* app);
    explicit MyTranslator();
    static MyTranslator* Instance();




    QString getEmptyString() { return ""; }


signals:
    void languageChanged();


public slots:
    void updateLanguage(ProjectCommon::LanguageEnum lang);


private:
    QGuiApplication* m_App;
    QTranslator m_Translator;
    static MyTranslator* m_pInstance;

};
