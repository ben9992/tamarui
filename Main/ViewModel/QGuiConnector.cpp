#include "QGuiConnector.h"

void TamarUi::QGuiConnector::RegisterTypes()
{
    RegisterType<TamarUi::QConstants>("Constants");
    RegisterType<TamarUi::QGuiConnector>("GuiConnector");
    RegisterType<TamarUi::QGuiTypes>("GuiTypes");
    RegisterType<ProjectCommon>("ProjectCommon");
    RegisterType<TamarUi::QGlobalConfiguration>("Configuration");
}

TamarUi::QGuiConnector::QGuiConnector(const Database::DataSet& memoryDataset, QObject *parent) :
    QDatabaseConnector(parent),
    m_constants(nullptr),
    m_configuration(nullptr),
    m_datasetMemory(memoryDataset)
{
    RegisterTypes();
}

TamarUi::QGuiConnector::QGuiConnector(QObject *parent) :
    QGuiConnector(nullptr, parent)
{
}

TamarUi::QGuiConnector::~QGuiConnector()
{
}

void TamarUi::QGuiConnector::Init()
{
    m_constants = new TamarUi::QConstants(this);
    m_configuration = new TamarUi::QGlobalConfiguration(m_datasetMemory, Context(),this);

    ProjectCommon::ParamsStruct stParams;
    m_datasetMemory[ProjectCommon::MonitorDBIndex::ParamsDefsStoreDBEnum][ParamStore::TAMAR_PARAMS_DEFS].Read<ProjectCommon::ParamsStruct>(stParams);
    m_configuration->set_videoChannelName1(stParams.m_strVideoChannelName1);
    m_configuration->set_videoChannelName2(stParams.m_strVideoChannelName2);
    m_configuration->set_videoChannelName3(stParams.m_strVideoChannelName3);
    m_configuration->set_videoChannelName4(stParams.m_strVideoChannelName4);
    m_configuration->set_videoChannelName5(stParams.m_strVideoChannelName5);
    m_configuration->set_webClientUrl(stParams.m_strWebClientUrl);

}

void TamarUi::QGuiConnector::Start()
{
}

void TamarUi::QGuiConnector::Stop()
{
    m_subscriptions.Clear();
}
