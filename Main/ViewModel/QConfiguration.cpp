#include "QConfiguration.h"

TamarUi::QGlobalConfiguration::QGlobalConfiguration(const Database::DataSet &dataset, const Utils::Context &context, QObject *parent) :
    QDatabaseConnector(context, parent),
    m_videoChannelName1(""),
    m_videoChannelName2(""),
    m_videoChannelName3(""),
    m_videoChannelName4(""),
    m_videoChannelName5(""),
    m_webClientUrl(""),
    m_dataset(dataset)
{
}

TamarUi::QGlobalConfiguration::QGlobalConfiguration(QObject *parent) :
    QGlobalConfiguration(nullptr, nullptr, parent)
{
}
