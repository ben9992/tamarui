#include "MyTranslator.h"

MyTranslator* MyTranslator::m_pInstance = NULL;


MyTranslator* MyTranslator::Instance()
{
    if(NULL == m_pInstance)
        m_pInstance = new MyTranslator();

    return m_pInstance;
}

MyTranslator::MyTranslator()
{
    if(NULL == m_pInstance)
        m_pInstance = new MyTranslator();
}

MyTranslator::MyTranslator(QGuiApplication* app)
{
    m_App = app;
    m_pInstance = this;
}


void
MyTranslator::updateLanguage(ProjectCommon::LanguageEnum lang){

    QString locale = QLocale::system().name();
    switch(lang){
    case ProjectCommon::ENGLISH_LANGUAGE:
         m_App->removeTranslator(&m_Translator);
        break;
    /*
    case ProjectProjectCommon::HEBREW_LANGUAGE:
       if (m_Translator.load("ut30Gui.qm","..\\Resource\\Translator\\HEBREW")!= true)
           printf("failed to load translation \n");
       m_App->installTranslator(&m_Translator);
        break;
     */
    case ProjectCommon::CROATIAN_LANGUAGE:
       if (m_Translator.load("MrssGui.qm","../Translations/CROATIAN")!= true)
           printf("failed to load translation \n");
       m_App->installTranslator(&m_Translator);
        break;
    case ProjectCommon::RUMAINIAN_LANGUAGE:
       if (m_Translator.load("MrssGui.qm","../Translations/RUMAINAIN")!= true)
           printf("failed to load translation \n");
       m_App->installTranslator(&m_Translator);
        break;
    default:
      // m_App->removeTranslator(&m_Translator);
        break;
    }
    emit languageChanged();
}
