#pragma once
#include <QObject>
#include <QMetaType>
#include "Helpers/QQmlAutoPropertyHelpers.h"
#include "ProjectCommon.h"

struct Coordinate
{
    Q_GADGET
    Q_PROPERTY(double x  MEMBER x)
    Q_PROPERTY(double y  MEMBER y)
    Q_PROPERTY(double z  MEMBER z)

public:
    double x;
    double y;
    double z;

    explicit Coordinate() :
        x(0.0), y(0.0), z(0.0)
    {
    }

    explicit Coordinate(double _x, double _y, double _z) :
        x(_x), y(_y), z(_z)
    {
    }

    Coordinate(const Coordinate& other) :
        x(other.x), y(other.y), z(other.z)
    {
    }

    virtual ~Coordinate()
    {
    }

    Coordinate& operator=(const Coordinate& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;

        return *this;
    }

    bool operator!=(const Coordinate& other) const
    {
        return (x != other.x ||
                y != other.y ||
                z != other.z);
    }

    bool operator==(const Coordinate& other) const
    {
        return !(*this != other);
    }
};
Q_DECLARE_METATYPE(Coordinate)

struct Orientation
{
    Q_GADGET
    Q_PROPERTY(double azimuth  MEMBER azimuth)
    Q_PROPERTY(double elevation  MEMBER elevation)
    Q_PROPERTY(double roll  MEMBER roll)

public:
    double azimuth;
    double elevation;
    double roll;

    explicit Orientation() :
        azimuth(0.0), elevation(0.0), roll(0.0)
    {
    }

    explicit Orientation(double _azimuth, double _elevation, double _roll) :
        azimuth(_azimuth), elevation(_elevation), roll(_roll)
    {
    }

    Orientation(const Orientation& other) :
        azimuth(other.azimuth), elevation(other.elevation), roll(other.roll)
    {
    }

    virtual ~Orientation()
    {
    }

    Orientation& operator=(const Orientation& other)
    {
        azimuth = other.azimuth;
        elevation = other.elevation;
        roll = other.roll;

        return *this;
    }

    bool operator!=(const Orientation& other) const
    {
        return (azimuth != other.azimuth ||
                elevation != other.elevation ||
                roll != other.roll);
    }

    bool operator==(const Orientation& other) const
    {
        return !(*this != other);
    }
};
Q_DECLARE_METATYPE(Orientation)

namespace TamarUi
{
    class QGuiTypes : public QObject
    {
        Q_OBJECT

    public:
        explicit QGuiTypes(QObject* parent = nullptr);

        enum ScreenTypeEnum
        {
            SCREEN_NONE = -1,
            SCREEN_HOME_SCREEN,
            SCREEN_BATTLE,
            SCREEN_BATTLE_SETTINGS,
            SCREEN_MAINTENANCE,
            SCREEN_BORESIGHT,
            SCREEN_CALIBRATION,
            SCREEN_MISSILE,
            SCREEN_FIRE_CORRECTION,
            SCREEN_OBSERVATION,
            GUI_NOF_SCREENS
        };
        Q_ENUM(ScreenTypeEnum)

        enum ReticleColorEnum
        {
            COLOR_BLACK = 0,
            COLOR_WHITE = 1,
            COLOR_RED   = 2
        };
        Q_ENUM(ReticleColorEnum)

        enum VMDStatusEnum
        {
            VMD_OFF = 0,
            VMD_ON = 1,
            VMD_DETECTION   = 2,
            VMD_DETECTION_OTHER_SENS_NIGHT   = 3,
            VMD_DETECTION_OTHER_SENS_DAY = 4
        };
        Q_ENUM(VMDStatusEnum)

        enum ObjectTypeEnum
        {
            POI,
            TARGET,
            MARK,
            TRACK,
            VMD
        };
        Q_ENUM(ObjectTypeEnum)

        /*Example*/
//		enum PolarityStates
//        {
//          Hot_White,
//          Hot_Black
//        };
//        Q_ENUM(PolarityStates)


    };
}
