import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtWebView 1.1
import QtQuick.Controls 2.12
import "../UiWidgets" as MyWidgets
import "../"
Item
{
    property alias brightnessValue:changeBrightness.value//binding the property to the slider value
    property alias contrastValue:changeContrast.value//binding the property to the slider value
    Rectangle
    {
        id:menuRectangle
        y:-100
        x:-270
        height:UiSystemDefs.buttonHeight
        width:UiSystemDefs.buttonWidth*3+40
        color: SystemDefs.transparentColor
        Image
        {
            z:-1//background
            id:mainMenuRegular
            anchors.fill: parent
            source:DesignStore.img_vidOptionsBackground_icn
        }
        Image
        {
            id:brightnessIconId
            x:parent.width-width - 15
            y:0
            height:35
            width:35
            source:  DesignStore.img_Brightness_icn
        }
        ColorOverlay
        {
            anchors.fill:brightnessIconId
            source: brightnessIconId
            color: UiSystemDefs.whiteColor //changing image color to white
        }

        Slider//brightness slider, from -1 to 1
        {
            id:changeBrightness
            x:40
            y:0
            from:-1
            value:0
            to:1
        }
        Image
        {
            id:contrastIconId
            x:parent.width-width-15
            y:parent.height-height
            height:35
            width:35
            source:  DesignStore.img_Contrast_icn
        }
        ColorOverlay
        {
            anchors.fill:contrastIconId
            source: contrastIconId
            color: UiSystemDefs.whiteColor //changing image color to white
        }

        Slider//brightness slider, from -1 to 1
        {
            id:changeContrast
            x:40
            y:parent.height-height
            from:-1
            value:0
            to:1
        }
        Image
        {
            id:resetBrightness
            source:DesignStore.img_reset_icn
            height:40
            width:40
            x:0
            y:0
            opacity:resetBrightnessMA.containsMouse ? 1 : 0.7//changing opacity if mouse cursor hover
            MouseArea
            {
                id:resetBrightnessMA
                hoverEnabled: true
                anchors.fill: parent
                onReleased: changeBrightness.value=0
            }
        }
        ColorOverlay
        {
            opacity:resetBrightnessMA.containsMouse ? 1 : 0.7//changing opacity if mouse cursor hover
            anchors.fill:resetBrightness
            source: resetBrightness
            color: UiSystemDefs.whiteColor //changing image color to white
        }
        Image
        {
            id:resetContrast
            source:DesignStore.img_reset_icn
            height:40
            width:40
            x:0
            y:parent.height-height
            opacity:resetContrastMA.containsMouse ? 1 : 0.7//changing opacity if mouse cursor hover
            MouseArea
            {
                id:resetContrastMA
                hoverEnabled: true
                anchors.fill: parent

                onReleased: changeContrast.value=0
            }
        }
        ColorOverlay
        {
            opacity:resetContrastMA.containsMouse ? 1 : 0.7//changing opacity if mouse cursor hover
            anchors.fill:resetContrast
            source: resetContrast
            color: UiSystemDefs.whiteColor //changing image color to white
        }
    }
}

