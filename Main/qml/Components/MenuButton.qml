import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtWebView 1.1
import "../UiWidgets" as MyWidgets
import "../"
Item
{

    property string iconPressedType:""  //icon when clicked
    property string iconRegularType:""  //regular icon
    property string iconDisabledType:""  //disabled icon
    property bool isDisabled: false
    signal btnClicked
    property bool currState  //current state of button, clicked or not
    property alias buttonText:bottomText.text
    Rectangle
    {
        height:UiSystemDefs.buttonHeight
        width:UiSystemDefs.buttonWidth
        color: SystemDefs.transparentColor
        Image
        {
            width:70
            height:70
            anchors.centerIn: parent
            id:buttonImage
            source:  currState ? iconPressedType: iconRegularType

        }
        Image
        {
            z:-1
            id:mainMenuRegular
            anchors.fill:parent
            //changing image depends on button status
            source:isDisabled ? DesignStore.img_buttonDisabled_icn : currState ?DesignStore.img_buttonActivated_icn:DesignStore.img_buttonRegular_icn
        }
        Text
        {
            id: bottomText
            font.family: UiSystemDefs.systemFont; font.pointSize: UiSystemDefs.systemFontSize; font.bold: true
            anchors
            {
                horizontalCenter:(iconRegularType==="" && iconDisabledType==="" && iconPressedType==="") ? undefined: parent.horizontalCenter
                bottom:(iconRegularType==="" && iconDisabledType==="" && iconPressedType==="")? undefined: parent.bottom
                centerIn:(iconRegularType==="" && iconDisabledType ===""&& iconPressedType==="") ? parent:undefined
                bottomMargin:10
            }

            color: isDisabled ? UiSystemDefs.greyColor : currState ? UiSystemDefs.blackColor : UiSystemDefs.whiteColor
        }
        MouseArea
        {
            id:menuArea
            visible: !isDisabled //if the button is not disabled
            anchors.fill: parent
            onReleased: btnClicked()
        }
    }


}
