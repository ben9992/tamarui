import QtQuick 2.4
import QtGraphicalEffects 1.0
import QtWebView 1.1
import "../UiWidgets" as MyWidgets
import "../Components" as Components
import "../"
Item {
    id: resizeRectItem
    signal itemSelect
    signal closeClicked //close button clicked
    property real rectWidth
    property real rectHeight
    property real initialRectWidth
    property real initialRectHeight
    property string publisherName: ""
    property string urlPath: ""
    property int typeOfItem: 0
    property bool isResizeable: false
    property bool isMoveable: true
    property int fillMode: UiSystemDefs.crop
    property alias brightnessOfVideo:videoSettingsMenu.brightnessValue//getting the value from the binded property
    property double contrastOfVideo:videoSettingsMenu.contrastValue//getting the value from the binded property
    rectWidth: rightWall.x - leftWall.x
    rectHeight: bottomWall.y - topWall.y
    Rectangle
    {
        id: resizeRect
        width: resizeRectItem.rectWidth
        height: resizeRectItem.rectHeight
        anchors.right: rightWall.left
        anchors.bottom: bottomWall.top
        anchors.top: topWall.bottom
        anchors.left: leftWall.right
        MyWidgets.UiVideoWgt
        {
            id:video
            visible: typeOfItem === UiSystemDefs.floatingVideo
            width: resizeRectItem.rectWidth
            height: resizeRectItem.rectHeight
            publisherName: resizeRectItem.publisherName
            fillMode:   resizeRectItem.fillMode
            MouseArea
            {
                id: setTopVideoId
                anchors.fill: parent
                onPressed: resizeRectItem.itemSelect()
            }
            Image
            {
                id:videoIcon
                visible: testTimer.running
                width: 30
                height : 30
                y:20
                source:DesignStore.img_target_icn
            }
            ColorOverlay
            {
                color: UiSystemDefs.whiteColor
                visible: testTimer.running
                source: videoIcon
                anchors.fill: videoIcon
            }
            Timer
            {
                id:testTimer
                repeat: true
                onTriggered:
                {
                    if(videoIcon.x < parent.width)
                        videoIcon.x = videoIcon.x + 10
                    else
                    {
                        videoIcon.x = 0
                        videoIcon.y = videoIcon.y + 10
                        if(videoIcon.y > parent.height)
                            videoIcon.y = 0
                    }

                }
                interval: 30
                running: UiSystemDefs.toggleTimer
            }
        }
        BrightnessContrast
        {
            id: brightnessVideo
            anchors.fill:video
            source:video
            brightness: resizeRectItem.brightnessOfVideo
            contrast: resizeRectItem.contrastOfVideo
        }
        WebView
        {
            visible: typeOfItem === UiSystemDefs.floatingWebClient
            id: webView
            width: parent.width
            height: parent.height
            url: urlPath
        }

        BrightnessContrast
        {
            id: brightnessWeb
            anchors.fill:webView
            source:webView
            brightness: resizeRectItem.brightnessOfVideo
            contrast: resizeRectItem.contrastOfVideo
        }
        Rectangle
        {
            id:videoTitleId
            //anchors.top: topWall.bottom
            width: parent.width
            height: 20
            color: UiSystemDefs.blackColor
            opacity: 0.8
            Text {
                id: videoTitle
                anchors.fill: parent
                fontSizeMode: Text.Fit
                font.pixelSize: 10000 // maximum height of the font
                minimumPixelSize: 8 // minimum height of the font
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                text: typeOfItem === UiSystemDefs.floatingVideo ? StringStore.string_PUBLISHER_NAME + publisherName : StringStore.string_URL_PATH + urlPath
                color: UiSystemDefs.whiteColor
            }
        }
    }
    //responsible on changing the video settings: brightness and contrast
    Rectangle
    {
        id:menuButton
        width: 70
        height: 70
        y: resizeRect.y + resizeRectItem.rectHeight - height
        x: resizeRect.x + resizeRectItem.rectWidth - width
        color: SystemDefs.transparentColor
        Image
        {
            anchors.fill: parent
            id: menuIcon
            //changing image depends on button status
            source: menuRectMA.pressed ? DesignStore.img_vidOptionsSel_icn : DesignStore.img_vidOptionsReg_icn
            MouseArea
            {
                id: menuRectMA
                anchors.fill: parent
                onReleased:
                {
                    if (typeOfItem==UiSystemDefs.floatingVideo) //if floating video
                    {
                        if(videoSettingsMenu.visible)
                        {
                            videoSettingsMenu.visible=false //hide the button
                        }
                        else if(!videoSettingsMenu.visible)
                        {
                            videoSettingsMenu.visible=true //show the button
                        }
                    }
                    else if(typeOfItem==UiSystemDefs.floatingWebClient)//if web client
                    {
                        if(webClientSettingsMenu.visible)
                        {
                            webClientSettingsMenu.visible=false //hide the button
                        }
                        else if(!webClientSettingsMenu.visible)
                        {
                            webClientSettingsMenu.visible=true //show the button
                        }
                    }
                }
            }
        }
        //settings
        Components.VideoOptionsMenu
        {
            id:videoSettingsMenu
            visible: false
        }
        Components.WebClientOptionsMenu
        {
            id:webClientSettingsMenu
            visible:false
            onReloadPage:webView.reload() //reloading the web client
        }
    }

    //responsible on resizing the video
    Rectangle
    {
        width: 70
        height: 70
        y: resizeRect.y
        x: resizeRect.x + resizeRectItem.rectWidth - width
        color: SystemDefs.transparentColor
        Image {
            anchors.fill: parent
            id: resizeIcon
            //changing image depends on button status
            source: resizeRectMA.pressed ? DesignStore.img_resizeSel_icn : DesignStore.img_resizeReg_icn
            MouseArea
            {
                id: resizeRectMA
                anchors.fill: parent
                property var lastX: 0
                property var lastY: 0
                property var lastWidth: 0
                property var lastHeight: 0
                property var lastIsMoveable: false
                property string currState: "minimize"
                onReleased:
                {
                    if(currState == "minimize")//setting video to fullsize
                    {
                        lastX = resizeRectItem.x //saving last x
                        lastY = resizeRectItem.y //saving last y
                        lastHeight = resizeRectItem.initialRectHeight
                        lastWidth = resizeRectItem.initialRectWidth
                        lastIsMoveable = resizeRectItem.isMoveable
                        resizeRectItem.isMoveable = false //cant drag the video
                        resizeRectItem.anchors.centerIn = resizeRectItem.parent //center of the screen
                        animationHei.start() //resize animation
                        animationWid.start()
                        resizeRectItem.z=1
                        resizeRectItem.itemSelect()
                        currState = "fullsize"
                        menuRectangle.visible=false
                    }
                    else //setting video to minimize
                    {
                        resizeRectItem.anchors.centerIn = null
                        animationInitHei.start()
                        animationInitWid.start()
                        animationInitX.start()
                        animationInitY.start()
                        resizeRectItem.z=0
                        resizeRectItem.isMoveable = lastIsMoveable
                        currState = "minimize"
                        menuRectangle.visible=true
                    }
                }
            }
        }
        //animating resize of the item
        PropertyAnimation { id: animationHei;running: false; target: resizeRectItem;property: "initialRectHeight";to: root.height - root.height / 1.9;duration: 500 }
        PropertyAnimation { id: animationWid;running: false; target: resizeRectItem;property: "initialRectWidth";to: root.width - root.width / 1.9;duration: 500 }
        PropertyAnimation { id: animationInitHei;running: false; target: resizeRectItem;property: "initialRectHeight";to: resizeRectMA.lastHeight;duration: 500 }
        PropertyAnimation { id: animationInitWid;running: false; target: resizeRectItem;property: "initialRectWidth";to: resizeRectMA.lastWidth;duration: 500 }
        PropertyAnimation { id: animationInitX;running: false; target: resizeRectItem;property: "x";to: resizeRectMA.lastX;duration: 500 }
        PropertyAnimation { id: animationInitY;running: false; target: resizeRectItem;property: "y";to: resizeRectMA.lasty;duration: 500 }
    }

    //responsible on dragging the video, only if isMoveable is true
    Rectangle
    {
        width: 70
        height: 70
        y: resizeRect.y + resizeRectItem.rectHeight - height
        x: resizeRect.x
        color: SystemDefs.transparentColor
        Image
        {
            anchors.fill: parent
            id: moveIcon
            //changing image depends on button status
            source: isMoveable ? resizeRectDragMA.pressed ? DesignStore.img_dragSel_icn : DesignStore.img_dragReg_icn : DesignStore.img_dragDisabled_icn
            MouseArea
            {
                id: resizeRectDragMA
                visible: isMoveable //visible if the video is moveable
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: containsMouse ? Qt.OpenHandCursor : Qt.ArrowCursor //changing mouse shape
                drag.target: resizeRectItem
                drag.axis: Drag.XandYAxis
                drag.maximumX: screen.width-resizeRect.width/2
                drag.minimumX: resizeRect.width/2
                drag.maximumY: screen.height-resizeRect.height/2
                drag.minimumY: resizeRect.height/2

                onPressed:
                {
                    resizeRectItem.anchors.centerIn = null
                    //resizeRectItem.itemSelect()
                    cursorShape = containsMouse ? Qt.ClosedHandCursor : Qt.ArrowCursor
                }
                onReleased: cursorShape = containsMouse ? Qt.OpenHandCursor : Qt.ArrowCursor
            }
        }
    }
    //responsible on closing the video
    Rectangle
    {
        width: 70
        height: 70
        y: resizeRect.y
        x: resizeRect.x
        color: SystemDefs.transparentColor
        Image
        {
            anchors.fill: parent
            id: exitIcon
            //changing image depends on button status
            source: exitRectMA.pressed ? DesignStore.img_deleteSel_icn : DesignStore.img_deleteReg_icn
            MouseArea {
                id: exitRectMA
                anchors.fill: parent
                onReleased: closeClicked()//calling closeClicked to set current video as false
            }
        }
    }

    //Resize FloatingItem on sides
    Rectangle
    {
        visible: isResizeable
        id: topWall
        x: -resizeRectItem.initialRectWidth
        y: -resizeRectItem.initialRectHeight
        width: resizeRectItem.rectWidth
        height: 4
        color: "red"
    }
    MouseArea //Resize FloatingItem on sides - click and drag area
    {
        visible: isResizeable
        id: topWallMA
        anchors.fill: topWall
        drag.target: topWall
        drag.axis: Drag.YAxis
        hoverEnabled: true
        cursorShape: containsMouse ? Qt.SizeVerCursor : Qt.ArrowCursor
        onPressed: {
            leftWall.anchors.top = topWall.bottom
            rightWall.anchors.top = topWall.bottom
        }
    }

    //Resize FloatingItem on sides
    Rectangle
    {
        visible: isResizeable
        id: bottomWall
        x:-resizeRectItem.initialRectWidth
        y: resizeRectItem.initialRectHeight
        width: resizeRectItem.rectWidth
        height: 4
        color: "purple"
    }
    MouseArea //Resize FloatingItem on sides - click and drag area
    {
        visible: isResizeable
        id: bottomWallMA
        anchors.fill: bottomWall
        drag.target: bottomWall
        drag.axis: Drag.YAxis
        hoverEnabled: true
        cursorShape: containsMouse ? Qt.SizeVerCursor : Qt.ArrowCursor
        onPressed: {
            leftWall.anchors.bottom = bottomWall.top
            rightWall.anchors.bottom = bottomWall.top
        }
    }

    //Resize FloatingItem on sides
    Rectangle
    {
        visible: isResizeable
        id: leftWall
        x: -resizeRectItem.initialRectWidth
        y: -resizeRectItem.initialRectHeight
        width: 4
        height: resizeRectItem.rectHeight
        color: "yellow"
    }
    MouseArea //Resize FloatingItem on sides - click and drag area
    {
        visible: isResizeable
        id: leftWallMA
        anchors.fill: leftWall
        drag.target: leftWall
        hoverEnabled: true
        cursorShape: containsMouse ? Qt.SizeHorCursor : Qt.ArrowCursor
        drag.axis: Drag.XAxis
        onPressed: {
            topWall.anchors.left = leftWall.right
            bottomWall.anchors.left = leftWall.right
        }
    }

    //Resize FloatingItem on sides
    Rectangle
    {
        visible: isResizeable
        id: rightWall
        x: resizeRectItem.initialRectWidth
        y: -resizeRectItem.initialRectHeight
        width: 4
        height: resizeRectItem.rectHeight
        color: "orange"
    }
    MouseArea //Resize FloatingItem on sides - click and drag area
    {
        visible: isResizeable
        id: rightWallMA
        anchors.fill: rightWall
        drag.target: rightWall
        hoverEnabled: true
        cursorShape: containsMouse ? Qt.SizeHorCursor : Qt.ArrowCursor
        drag.axis: Drag.XAxis
        onPressed: {
            topWall.anchors.right = rightWall.left
            bottomWall.anchors.right = rightWall.left
        }
    }


    states: [
        State {
            name: "hide"
            PropertyChanges {
                target: resizeRectItem
                opacity: 0.0
            }
        }
    ]
}
