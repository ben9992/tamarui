import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtWebView 1.1
import QtQuick.Controls 2.12
import "../UiWidgets" as MyWidgets
import "../"

Item
{
    signal reloadPage
    Rectangle
    {
        id:menuRectangle
        y:-40
        x:-140
        height:UiSystemDefs.buttonHeight/2
        width:UiSystemDefs.buttonWidth*2
        color: SystemDefs.transparentColor
        Image
        {
            z:-1//background
            id:mainMenuRegular
            anchors.fill: parent
            source:DesignStore.img_vidOptionsBackground_icn
        }
        Image
        {
            id:refreshIcon
            x:parent.width-width - 15
            y:parent.height/2-height/2
            height:35
            width:35
            source:  DesignStore.img_refresh_icn
            MouseArea
            {
                id:refreshIconMA
                hoverEnabled: true
                anchors.fill: parent
                onReleased: reloadPage() //reloading the page when released
            }
        }
        ColorOverlay
        {
            opacity:refreshIconMA.containsMouse ? 1 : 0.7
            anchors.fill:refreshIcon
            source:refreshIcon
            color: UiSystemDefs.whiteColor
        }
    }
}
