//====================================================================================//
//
//                             UiDefs
//
//      General global definitions
//
//====================================================================================//

pragma Singleton

import QtQuick 2.9
//import MyProjectQMLEnums 1.0
import TamarUi 1.0

// SystemDefs.
QtObject {
    property bool toggleTimer: false
    property int floatingVideo:                     1
    property int floatingWebClient:                 2

    //----------- Enum for index of pairs for coordinates or size: ---------//

    //----------- Enum for index of pairs for coordinates or size: ---------//
    property int    iX:                             0
    property int    iY:                             1
    property int    iWidth:                         0
    property int    iHeight:                        1

    //----------- Buttons Type Enum ------------------------------//
    property int buttonStatusDisabled:              0
    property int buttonStatusEnabled:               1
    //----------- Layout Type Enum ------------------------------//
    property int currLayout: defaultLayout
    property int defaultLayout:                     1
    property int battleLayout:                      2

    //----------- Preserve Aspect Enum ------------------------------//
    property int stretch:                           0
    property int crop:                              1
    property int fit:                               2

    //----------- Buttons Direction------------------------------//

    property int buttonDirectionLeft:               0
    property int buttonDirectionRight:              2
    property int buttonDirectionRightDown:          3



    property int buttonPositionCenter:              0
    property int buttonPositionDown:                1

    ////Button Emun/////////////
    property int buttonPressing:             3
    property int buttonPress:                2
    property int buttonUnPress:              1

    //----------- comboBox Direction------------------------------//
    property int directionUp:                0
    property int directionDown:              1

    //----------- comboBox status------------------------------//
    property int comoboBoxClose:              0
    property int comoboBoxOpen:              1
    //----------- Colors ------------------------------//
    property string transparentColor:       "transparent"
    property string transparentColorEx:      "#101010"
    property string greyColor:               "grey"
    property string whiteColor:              "white"
    property string blackColor:              "black"
    property string blueBackgroundColor:             "#16113a"

    //----------- Fonts ------------------------------//
    property string systemFont:             "qrc:/Configuration/Japan/fonts/RobotoMono.ttf"
    property int systemFontSize:             12
    property bool   isDeclutOn:              false

    property bool   isBallisticTableOn:      false
    property bool   isAECOn:                 false
    property bool   isNUCOn:                 false
    property bool   isScanPOn:               false

    property bool    bReticleDisplay: true
    property int     nReticleColor: GuiTypes.COLOR_WHITE
    property int    buttonHeight:               100
    property int    buttonWidth:               100

    property int     maxNumberOfMslToOperMsg: 39

    property bool    missileMsgBoxVisible: false


    //----------- full screen resolution ----------------------//

    function rad2Deg(val)
    {
        return val*  (360.0/(2*Math.PI))
    }
    function mrad2DEG(val)
    {
        return val*(360.0/(2000*Math.PI))
    }
    function rad2Mills(val)
    {
        return val*(6400.0/(2*Math.PI))
    }

  }
