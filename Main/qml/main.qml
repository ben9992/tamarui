import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.0
import qmlReload 1.0
import TamarUi 1.0
import "./" as UiScreens
import "./UiWidgets" as MyWidgets
Window
{
    readonly property int screen_id: 0//1 //laptop
    readonly property int number_of_physical_screens: 1
    readonly property int number_of_logical_screens: 1
    readonly property int screen_width: (Qt.application.screens[screen_id].width * number_of_physical_screens) / number_of_logical_screens
    readonly property int screen_height: (Qt.application.screens[screen_id].height * number_of_physical_screens) / number_of_logical_screens

    id: root
    flags: Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint | Qt.Window

    visible: true

    //    Laptop
    //    width: screen_width * number_of_logical_screens + 1700
    //    height: 1200

    //    x: -400//Qt.application.screens[screen_id].virtualX + screen_width - 950
    //    y: -450//Qt.application.screens[screen_id].virtualY + ((Qt.application.screens[screen_id].height - height) / 2);

    width: Screen.width//screen_width
    height: Screen.height
    color: UiSystemDefs.blackColor
    x: 0//Qt.application.screens[screen_id].virtualX - screen_width
    y: 0

    UiScreens.UiMainWindow
    {
        id:uiMainWindow
        anchors.fill: parent
    }




    // Debugging helpers

    QmlReload
    {
        id: reloader
    }
    Row
    {
        x:0
        y:0
        spacing: 10
        Button
        {
            x:0// screen_width + screen_width - width
            y:0// parent.height - 25
            width: 100
            height: 25
            text: "Reload!"

            onClicked:
            {
                reloader.reload()
            }
        }
        Button
        {
            width: 100
            height: 25
            text: "play icon"

            onClicked:
            {
                UiSystemDefs.toggleTimer = true;
            }
        }

        Button
        {
            width: 100
            height: 25
            text: "stop icon"

            onClicked:
            {
                UiSystemDefs.toggleTimer = false;
            }
        }
    }

}
