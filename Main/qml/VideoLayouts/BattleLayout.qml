//====================================================================================//
//
//                             UiMainWindow
//
//      This is the Main Window item. It holds all widgets and screens
//
//====================================================================================//

import QtQuick 2.9
import QtQuick.Controls 1.2
import QtMultimedia 5.5
import qmlReload 1.0
import TamarUi 1.0
import "../Components" as Components
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.12
import "../"
import "../UiWidgets" as MyWidgets
import "../UiScreens" as MyScreens

Item
{
    id : defaultLayoutId
    property int marginItems: UiVideoDefs.marginSize
    Components.FloatingItem
    {
        id:video1
        visible: UiVideoDefs.video1Visible //video visibility is dependent on the boolean for that video's visible status
        publisherName : guiConnector.configuration.videoChannelName1
        typeOfItem: UiSystemDefs.floatingVideo
        initialRectWidth: (parent.width - 3*marginItems) / 2//calculating video size
        initialRectHeight: (parent.height - 3*marginItems) / 4
        x: initialRectWidth + marginItems
        y: initialRectHeight + marginItems
        isMoveable: false
        fillMode: UiSystemDefs.crop
        onCloseClicked:
        {
            UiVideoDefs.video1Visible=false //if the close button clicked-video visibility is false
        }
    }

    Components.FloatingItem {
        id:video2
        visible: UiVideoDefs.video2Visible //video visibility is dependent on the boolean for that video's visible status
        publisherName : guiConnector.configuration.videoChannelName2
        typeOfItem: UiSystemDefs.floatingVideo
        initialRectWidth: calcWidthSize()
        initialRectHeight: calcHeightSize()
        x:initialRectWidth + marginItems
        y:parent.height - initialRectHeight - marginItems
        isMoveable: false
        fillMode: UiSystemDefs.crop
        onCloseClicked:
        {
            UiVideoDefs.video2Visible=false //if the close button clicked-video visibility is false
        }
    }

    Components.FloatingItem {
        id:webClient
        visible:UiVideoDefs.webClientVisible //web client visibility is dependent on the boolean for web client visible status
        urlPath: guiConnector.configuration.webClientUrl
        typeOfItem: UiSystemDefs.floatingWebClient
        initialRectWidth: calcWidthSize()
        initialRectHeight: calcHeightSize()
        x:parent.width - initialRectWidth - marginItems
        y:parent.height - initialRectHeight - marginItems
        isMoveable: false
        fillMode: UiSystemDefs.crop
        onCloseClicked:
        {
            UiVideoDefs.webClientVisible=false //if the close button clicked-video visibility is false
        }
    }

    Components.FloatingItem {
        id:video4
        visible:UiVideoDefs.video4Visible //video visibility is dependent on the boolean for that video's visible status
        publisherName : guiConnector.configuration.videoChannelName4
        typeOfItem: UiSystemDefs.floatingVideo
        initialRectWidth: calcWidthSize() / 1.5
        initialRectHeight: calcHeightSize() / 1.5
        x:parent.width / 2 - 4*80 - initialRectWidth
        y:parent.height /2 - initialRectHeight *1.6
        fillMode: UiSystemDefs.crop
        onCloseClicked:
        {
            UiVideoDefs.video4Visible=false //if the close button clicked-video visibility is false
        }
    }

    Components.FloatingItem {
        id:video5
        visible:UiVideoDefs.video5Visible //video visibility is dependent on the boolean for that video's visible status
        publisherName : guiConnector.configuration.videoChannelName5
        typeOfItem: UiSystemDefs.floatingVideo
        initialRectWidth: calcWidthSize() / 1.5
        initialRectHeight: calcHeightSize() / 1.5
        x:parent.width / 2 + 3*80 + initialRectWidth
        y:parent.height / 2 - initialRectHeight *1.6
        fillMode: UiSystemDefs.crop
        onCloseClicked:
        {
            UiVideoDefs.video5Visible=false //if the close button clicked-video visibility is false
        }
    }

    function calcWidthSize()
    {
        return (parent.width - 3*marginItems) / 4
    }

    function calcHeightSize()
    {
        return (parent.height - 3*marginItems) / 4
    }
}
