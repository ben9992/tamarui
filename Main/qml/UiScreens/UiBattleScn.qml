import QtQuick 2.9
import TamarUi 1.0
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
//import MyProjectQMLEnums 1.0


import "./../UiWidgets" as MyWidgets
import "./../UiScreens" as MyScreens
import "../UiButtons" as MyButtons
import "./../"


Item {

    anchors.fill: parent

    id: battleScnId
     MyWidgets.UiVideoWgt
     {
         z:-20
         width: Screen.width
         height: Screen.height
         publisherName: guiConnector.common.nActiveScreen === GuiTypes.SCREEN_BATTLE ? "suzi" : "none"
     }

    signal                          mainWindownButtonPress



}
