//====================================================================================//
//
//                             UiStringStore
//
//      String Store - holds all ui strings for the application
//
//====================================================================================//

pragma Singleton

import QtQuick 2.9

// StringStore.
QtObject {

    property bool isCurrentLanguageEnglish: true

    //Msgs Succes
    property string string_PRINTSCREEN                          : qsTr("SUCCESSFUL SCREEN PRINTING") //+MyTranslator.emptyString

    //Msgs Fail
    property string string_PRINTSCREEN_FAIL                     : qsTr("FAIL TO SCREEN PRINTING") //+MyTranslator.emptyString

    property string string_PUBLISHER_NAME                       : qsTr("Publisher Name: ") //+MyTranslator.emptyString
    property string string_URL_PATH                             : qsTr("Url Path: ") //+MyTranslator.emptyString
    property string string_web_client                           : qsTr("Web Client") //+MyTranslator.emptyString
    property string string_video                                : qsTr("Video") //+MyTranslator.emptyString

    property string string_layout                                      : qsTr("Layout:") //+MyTranslator.emptyString
    property string string_no_video                                      :qsTr("No Video")

    property string string_IRON_VISION                          : qsTr(" IRON\nVISION")    //+MyTranslator.emptyString
    property string string_IRON_VISION_PIP_ENABLE               : qsTr("PIP ENABLE")    //+MyTranslator.emptyString
    property string string_IRON_VISION_PIP_FOV_SIZE             : qsTr("PIP FOV SIZE")    //+MyTranslator.emptyString
    property string string_IRON_VISION_PIP_FOV_WIDTH            : qsTr("PIP FOV WIDTH")    //+MyTranslator.emptyString
    property string string_IRON_VISION_PIP_FOV_HEIGHT           : qsTr("PIP FOV HEIGHT")    //+MyTranslator.emptyString
    //groups name
    property variant groupNameText: [
        qsTr("GENERAL"),
        qsTr("FATAL"),
        qsTr("GUN"),
        qsTr("MAG"),
        qsTr("OPTICS"),
        qsTr("TCEU"),
        qsTr("TOU"),
        qsTr("EMU"),
        qsTr("EDSS"),
        qsTr("GCU"),
        qsTr("GNNR"),
        qsTr("FIRE"),
        qsTr("MAG"),
        qsTr("DEGRADATION"),
        qsTr("CMDR COAPS"),
        qsTr("GNNR COAPS"),
        qsTr("HIDDEN"),
        qsTr("LWS"),
    ]






    function toggleLanguage(){
        isCurrentLanguageEnglish = !isCurrentLanguageEnglish

        if(true === isCurrentLanguageEnglish){
            string_COMBAT    =	"COMBAT"
        }
        else{
            string_COMBAT    =	"לחימה"
        }
    }

}
