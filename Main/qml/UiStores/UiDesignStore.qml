//====================================================================================//
//
//                             UiDesignStore
//
//      Design Store - holds all pathes to Images in the application
//
//====================================================================================//

pragma Singleton

import QtQuick 2.9

// DesignStore.
QtObject {
    property string img_dragReg_icn                    : "qrc:/Configuration/Tamar/Images/FloatingItem/DragRegular.svg"
    property string img_dragDisabled_icn               : "qrc:/Configuration/Tamar/Images/FloatingItem/DragDisabled.svg"
    property string img_dragSel_icn                    : "qrc:/Configuration/Tamar/Images/FloatingItem/DragSelected.svg"
    property string img_resizeReg_icn                  : "qrc:/Configuration/Tamar/Images/FloatingItem/ResizeRegular.svg"
    property string img_resizeSel_icn                  : "qrc:/Configuration/Tamar/Images/FloatingItem/ResizeSelected.svg"
    property string img_deleteReg_icn                  : "qrc:/Configuration/Tamar/Images/FloatingItem/DeleteRegular.svg"
    property string img_deleteSel_icn                  : "qrc:/Configuration/Tamar/Images/FloatingItem/DeleteSelected.svg"
    property string img_outArrow_icn                    : "qrc:/Configuration/Tamar/Images/FloatingItem/outArrow.svg"
    property string img_inArrow_icn                   : "qrc:/Configuration/Tamar/Images/FloatingItem/inArrow.svg"
    property string img_Vi_icn                        : "qrc:/Configuration/Tamar/Images/FloatingItem/Vi.svg"
    property string img_close_icn                         : "qrc:/Configuration/Tamar/Images/FloatingItem/close.svg"
    property string img_mainMenuReg_icn               : "qrc:/Configuration/Tamar/Images/Menu/MainMenuRegular.svg"
    property string img_mainMenuDisabled_icn          : "qrc:/Configuration/Tamar/Images/Menu/MainMenuDisabled.svg"
    property string img_mainMenuActivated_icn               : "qrc:/Configuration/Tamar/Images/Menu/MainMenuActivated.svg"
    property string img_buttonActivated_icn           : "qrc:/Configuration/Tamar/Images/ActionButton/BtnBGActivated.svg"
    property string img_buttonRegular_icn             : "qrc:/Configuration/Tamar/Images/ActionButton/BtnBGRegular.svg"
    property string img_buttonDisabled_icn           :  "qrc:/Configuration/Tamar/Images/ActionButton/BtnBGDisabled.svg"
    property string img_videoReg_icn                 : "qrc:/Configuration/Tamar/Images/Menu/VideoRegular.svg"
    property string img_videoDisabled_icn            : "qrc:/Configuration/Tamar/Images/Menu/VideoDisabled.svg"
    property string img_videoActivated_icn           : "qrc:/Configuration/Tamar/Images/Menu/VideoActivated.svg"
    property string img_settingsReg_icn              : "qrc:/Configuration/Tamar/Images/Menu/TechnicianRegular.svg"
    property string img_settingsDisabled_icn         : "qrc:/Configuration/Tamar/Images/Menu/TechnicianDisabled.svg"
    property string img_settingsActivated_icn        : "qrc:/Configuration/Tamar/Images/Menu/TechnicianSelected.svg"
    property string img_layoutReg_icn              : "qrc:/Configuration/Tamar/Images/Menu/PresetRegular.svg"
    property string img_layoutDisabled_icn         : "qrc:/Configuration/Tamar/Images/Menu/PresetDisabled.svg"
    property string img_layoutActivated_icn        : "qrc:/Configuration/Tamar/Images/Menu/PresetActivated.svg"
    property string img_vidOptionsReg_icn              : "qrc:/Configuration/Tamar/Images/Menu/MenuRegular.svg"
    property string img_vidOptionsDisabled_icn         : "qrc:/Configuration/Tamar/Images/Menu/MenuDisabled.svg"
    property string img_vidOptionsSel_icn        : "qrc:/Configuration/Tamar/Images/Menu/MenuSelected.svg"
    property string img_Brightness_icn            : "qrc:/Configuration/Tamar/Images/FloatingItem/Brightness.svg"
    property string img_Contrast_icn            : "qrc:/Configuration/Tamar/Images/FloatingItem/Contrast.svg"
    property string img_vidOptionsBackground_icn     : "qrc:/Configuration/Tamar/Images/ActionButton/BtnBGFocused.svg"
    property string img_reset_icn                    : "qrc:/Configuration/Tamar/Images/FloatingItem/reset.svg"
    property string img_refresh_icn                  : "qrc:/Configuration/Tamar/Images/FloatingItem/refresh.svg"
    property string img_target_icn                   : "qrc:/Configuration/Tamar/Images/Icons/target.svg"
}
