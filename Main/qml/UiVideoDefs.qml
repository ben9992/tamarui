//====================================================================================//
//
//                             UiDefs
//
//      General global definitions
//
//====================================================================================//

pragma Singleton
import QtQuick 2.9
//import MyProjectQMLEnums 1.0
import TamarUi 1.0

// SystemDefs.
QtObject
{
    property bool video1Visible:true
    property bool video2Visible:true
    property bool video3Visible:true
    property bool video4Visible:true
    property bool video5Visible:true
    property bool webClientVisible:true
    property int marginSize:90
 }
