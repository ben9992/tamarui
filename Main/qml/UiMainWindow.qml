//====================================================================================//
//
//                             UiMainWindow
//
//      This is the Main Window item. It holds all widgets and screens
//
//====================================================================================//

import QtQuick 2.12
import QtQuick.Controls 1.2
import QtMultimedia 5.5
import qmlReload 1.0
import TamarUi 1.0

import "./Components" as Components
import QtQuick.Window 2.0
import QtGraphicalEffects 1.12
import QtQuick.Controls 2.12


import "./UiWidgets" as MyWidgets
import "./UiScreens" as MyScreens
import "./VideoLayouts" as Layouts
import "."

Rectangle
{
    //-----------------------------------------------------------//
    //                  properties
    //-----------------------------------------------------------//
    id:                             rootId
    color:                          SystemDefs.blueBackgroundColor
    width: parent.width
    height: parent.height
    Item
    {
        id : mainWindowId
    }

    Layouts.BattleLayout
    {
        width: parent.width
        height: parent.height
        visible: UiSystemDefs.currLayout === UiSystemDefs.battleLayout //battle layout if the curr layout is battle
    }

    Layouts.DefaultLayout
    {
        width: parent.width
        height: parent.height
        visible: UiSystemDefs.currLayout === UiSystemDefs.defaultLayout //default layout if the curr layout is default
    }

    Rectangle
    {
        height:70
        width:70
        x:screen.width-width
        color: SystemDefs.transparentColor
        Image
        {
            z:-1
            anchors.fill:parent
            //changing image depends on button status
            source:DesignStore.img_buttonRegular_icn
        }

        Image
        {
            id:closeButtonImage
            height:40
            width:40
            source: DesignStore.img_close_icn
            anchors.centerIn: parent

        }
        ColorOverlay
        {
            anchors.fill:closeButtonImage
            source: closeButtonImage
            color: UiSystemDefs.whiteColor //changing image color to white
        }
        MouseArea
        {
            id:closeButton
            anchors.fill:parent
            onReleased: Qt.quit()
        }


    }

    //Popup that includes the main menu
    Popup
    {
        id:menuPopup
        visible:true
        x:0 //bottom left corner of the screen
        y:screen.height-height-10
        height:parent.height/12
        width:parent.width/20
        background: Rectangle
        {
            color:UiSystemDefs.transparentColor//transparent background to hide the popup
        }
        closePolicy: mainMenu.isMenuDisabled? Popup.NoAutoClose: Popup.CloseOnPressOutside //close when user clicked outside of the popup
        onClosed:
        {
            mainMenu.triggerMenuOutAnimation()
            menuPopup.open() //reopen the popup
        }
        MyWidgets.UiMenuWgt
        {
            id:mainMenu
        }
    }
}




