import QtQuick 2.9
import QtQuick.Controls 2.12
import QtMultimedia 5.5
import QtQuick.Window 2.3
import "../Components" as Components
import ".././"

Item
{
    id:menuId
    property bool isMenuOpen: false//boolean for status of each button-open/not open
    property bool isVideoListOpen:false
    property bool isLayoutListOpen: false
    property bool isSettingsListOpen:false
    property bool isMenuDisabled:false
    property bool isVideosListDisabled:false
    property bool isLayoutListDisabled:false
    property bool isSettingsListDisabled:false


    x:-12//position on popup
    y:-10
    height:parent.height/12
    width:parent.width/20
    onIsMenuOpenChanged:
    {
        if(!isMenuOpen)//if menu closed
        {
            isVideoListOpen=false
            isLayoutListOpen=false
            isSettingsListOpen=false
        }
    }
    onIsMenuDisabledChanged:
    {
        isVideosListDisabled=isMenuDisabled
        isLayoutListDisabled=isMenuDisabled
        isSettingsListDisabled=isMenuDisabled
    }

    //menu buttn-opening the other menu buttons
    Components.MenuButton
    {
        id:menuBtnId
        isDisabled: isMenuDisabled
        iconPressedType:DesignStore.img_mainMenuActivated_icn//icon for each button status
        iconRegularType:DesignStore.img_mainMenuReg_icn
        iconDisabledType:DesignStore.img_mainMenuDisabled_icn
        currState:isMenuOpen//button clicked or not
        onBtnClicked:
        {
            if(currState==true)
            {

                videosListButtonOut.start()
                videosLayoutButtonOut.start()
                settingsButtonOut.start()

            }
            else if(currState==false)
            {
                isMenuOpen=true
                videosListButtonIn.start()
                videosLayoutButtonIn.start()
                settingsButtonIn.start()
            }
        }

    }
    //opening video list that lets you open/close videos
    Components.MenuButton
    {
        visible: isMenuOpen//visible if menu open
        id:videoListButtonId
        isDisabled: isVideosListDisabled
        iconPressedType:DesignStore.img_videoActivated_icn
        iconRegularType:DesignStore.img_videoReg_icn
        iconDisabledType:DesignStore.img_videoDisabled_icn
        currState:isMenuOpen && isVideoListOpen //clicked only if the menu is open and the video list is open
        onBtnClicked:
        {
            triggerLayoutButtonsOutAnimation()
            isSettingsListOpen=false//closing other buttons when clicked
            if(currState==true)
            {
                triggerVideoButtonsOutAnimation()
            }
            else if(currState==false)
            {
                isVideoListOpen=true
                triggerVideoButtonsInAnimation()

            }
        }
    }

    Components.MenuButton
    {
        id:video1button
        visible: isVideoListOpen //visible only if the video list is open
        isDisabled: isVideosListDisabled
        x:100
        y:-110
        buttonText: StringStore.string_video + " 1"
        currState:UiVideoDefs.video1Visible //true if video 1 is visible
        onBtnClicked:
        {
            if(currState==true) //if the button was clicked when shown
            {
                UiVideoDefs.video1Visible=false //hide video
            }
            else if(currState==false) //if the button was clicked when hidden
            {
                UiVideoDefs.video1Visible=true //show video
            }
        }
    }
    Components.MenuButton
    {
        id:video2button
        visible: isVideoListOpen //visible only if the video list is open
        isDisabled: isVideosListDisabled
        x:100
        y:-110
        buttonText: StringStore.string_video + " 2"
        currState:UiVideoDefs.video2Visible //true if video 2 is visible
        onBtnClicked:
        {
            if(currState==true) //if the button was clicked when shown
            {
                UiVideoDefs.video2Visible=false //hide video
            }
            else if(currState==false) //if the button was clicked when shown
            {
                UiVideoDefs.video2Visible=true //show video
            }
        }
    }
    Components.MenuButton
    {
        id:video3button
        visible: isVideoListOpen //visible only if the video list is open
        x:100
        y:-110
        isDisabled: isVideosListDisabled || UiSystemDefs.currLayout === UiSystemDefs.battleLayout //disabled if the layout is battle layout
        buttonText: StringStore.string_video + " 3"
        currState:UiVideoDefs.video3Visible //true if video 3 is visible
        onBtnClicked:
        {
            if(currState==true) //if the button was clicked when shown
            {
                UiVideoDefs.video3Visible=false //hide video
            }
            else if(currState==false) //if the button was clicked when shown
            {
                UiVideoDefs.video3Visible=true //show video
            }
        }
    }
    Components.MenuButton
    {
        id:video4button
        visible: isVideoListOpen //visible only if the video list is open
        isDisabled: isVideosListDisabled
         x:100
        y:-110
        buttonText: StringStore.string_video + " 4"
        currState:UiVideoDefs.video4Visible //true if video 4 is visible
        onBtnClicked:
        {
            if(currState==true) //if the button was clicked when shown
            {
                UiVideoDefs.video4Visible=false //hide video
            }
            else if(currState==false) //if the button was clicked when shown
            {
                UiVideoDefs.video4Visible=true //show video
            }
        }
    }
    Components.MenuButton
    {
        id:video5button
        visible: isVideoListOpen //visible only if the video list is open
        isDisabled: isVideosListDisabled
         x:100
        y:-110
        buttonText: StringStore.string_video + " 5"
        currState:UiVideoDefs.video5Visible //true if video 5 is visible
        onBtnClicked:
        {

            if(currState==true) //if the button was clicked when shown
            {
                UiVideoDefs.video5Visible=false //hide video
            }
            else if(currState==false) //if the button was clicked when shown
            {
                UiVideoDefs.video5Visible=true //show video

            }
        }
    }
    Components.MenuButton
    {
        id:webClientButton
        visible: isVideoListOpen//visible only if the video list is open
        isDisabled: isVideosListDisabled
        x:100
        y:-110
        buttonText: StringStore.string_web_client
        currState:UiVideoDefs.webClientVisible //true if the web client is visible
        onBtnClicked:
        {
            if(currState==true) //if the button was clicked when shown
            {
                UiVideoDefs.webClientVisible=false //hide web client
            }
            else if(currState==false) //if the button was clicked when shown
            {
                UiVideoDefs.webClientVisible=true //show web client
            }
        }
    }
    //button that opens the layouts options
    Components.MenuButton
    {
        id:videoLayoutListId
        visible: isMenuOpen//visible only if menu is visible
        isDisabled:isLayoutListDisabled
        iconPressedType:DesignStore.img_layoutActivated_icn
        iconRegularType:DesignStore.img_layoutReg_icn
        iconDisabledType:DesignStore.img_layoutDisabled_icn
        currState:isMenuOpen && isLayoutListOpen
        onBtnClicked:
        {
            triggerVideoButtonsOutAnimation()//closing other buttons
            isSettingsListOpen=false
            if(currState==true)
            {
                triggerLayoutButtonsOutAnimation()
            }
            else if(currState==false)
            {
                isLayoutListOpen=true
                triggerLayoutButtonsInAnimation()
            }
        }
    }
    //change to layout 1 button
    Components.MenuButton
    {
        id:layout1button
        visible: isLayoutListOpen
        isDisabled:isLayoutListDisabled
        y:-220
        buttonText: StringStore.string_layout + " 1"
        currState:UiSystemDefs.currLayout === UiSystemDefs.defaultLayout//true when the layout is the default layout
        onBtnClicked:
        {
            UiSystemDefs.currLayout = UiSystemDefs.defaultLayout
        }
    }
    //change to layout 2 button
    Components.MenuButton
    {
        id:layout2button
        visible: isLayoutListOpen
        y:-220
        buttonText: StringStore.string_layout + " 2"
        currState:UiSystemDefs.currLayout === UiSystemDefs.battleLayout//true when the layout is battle layout
        onBtnClicked:
        {
            UiSystemDefs.currLayout = UiSystemDefs.battleLayout
        }
    }
    //settings button
    Components.MenuButton
    {
        id:settingsListId
        visible: isMenuOpen
        iconPressedType:DesignStore.img_settingsActivated_icn
        iconRegularType:DesignStore.img_settingsReg_icn
        iconDisabledType:DesignStore.img_settingsDisabled_icn
        currState:isSettingsListOpen
        onBtnClicked:
        {
            triggerVideoButtonsOutAnimation()
            triggerLayoutButtonsOutAnimation()//closing other buttons when clicked
            if(currState==true)
            {
                isSettingsListOpen=false
            }
            else if(currState==false)
            {
                isSettingsListOpen=true
            }
        }
    }


    PropertyAnimation { id: videosListButtonIn;running: false; target: videoListButtonId;property: "y";from:-100; to: -110;duration: 300;
                        onStarted:{triggerVideoButtonsOutAnimation(); isMenuDisabled=true}}
    PropertyAnimation { id: videosLayoutButtonIn;running: false; target: videoLayoutListId;property: "y";from:-100; to: -220;duration: 300; onStarted:triggerLayoutButtonsOutAnimation() }
    PropertyAnimation { id: settingsButtonIn;running: false; target: settingsListId;property: "y";from:-100; to: -330;duration: 300; onStopped:isMenuDisabled=false}

    PropertyAnimation { id: videosListButtonOut;running: false; target: videoListButtonId;property: "y";from:-110; to: 0;duration: 300;
                        onStarted: { isMenuDisabled=true; isVideoListOpen=false }}
    PropertyAnimation { id: videosLayoutButtonOut;running: false; target: videoLayoutListId;property: "y";from:-220; to: 0;duration: 300; onStarted: isLayoutListOpen=false}
    PropertyAnimation { id: settingsButtonOut;running: false; target: settingsListId;property: "y";from: -330; to: 0;duration: 300;onStarted:isSettingsListOpen=false;
                        onStopped: { isMenuDisabled=false; isMenuOpen=false }}

    PropertyAnimation { id: video1buttonIn;running: false; target: video1button;property: "x";from:100;to: 110;duration: 300; onStarted: isVideosListDisabled=true}
    PropertyAnimation { id: video2buttonIn;running: false; target: video2button;property: "x";from:100;to: 220;duration: 300; }
    PropertyAnimation { id: video3buttonIn;running: false; target: video3button;property: "x";from:100;to: 330;duration: 300; }
    PropertyAnimation { id: video4buttonIn;running: false; target: video4button;property: "x";from:100;to: 440;duration: 300; }
    PropertyAnimation { id: video5buttonIn;running: false; target: video5button;property: "x";from:100;to: 550;duration: 300; }
    PropertyAnimation { id: webClientButtonIn;running: false; target: webClientButton;property: "x";from:100;to: 660;duration: 300;
                        onStopped: isVideosListDisabled=false}

    PropertyAnimation { id: video1buttonOut;running: false; target: video1button;property: "x";from:110;to: 0;duration: 300; onStarted: isVideosListDisabled=true}
    PropertyAnimation { id: video2buttonOut;running: false; target: video2button;property: "x";from:220;to: 0;duration: 300; }
    PropertyAnimation { id: video3buttonOut;running: false; target: video3button;property: "x";from:330;to: 0;duration: 300; }
    PropertyAnimation { id: video4buttonOut;running: false; target: video4button;property: "x";from:440;to: 0;duration: 300; }
    PropertyAnimation { id: video5buttonOut;running: false; target: video5button;property: "x";from:550;to: 0;duration: 300; }
    PropertyAnimation { id: webClientButtonOut;running: false; target: webClientButton;property: "x";from:660;to: 0;duration: 300;
                        onStopped: {isVideoListOpen=false; isVideosListDisabled=false}}

    PropertyAnimation { id: layout1buttonIn;running: false; target: layout1button;property: "x";from:100; to: 110;duration: 300; onStarted: isLayoutListDisabled=true}
    PropertyAnimation { id: layout2buttonIn;running: false; target: layout2button;property: "x";from:100; to: 220;duration: 300; onStopped: isLayoutListDisabled=false}

    PropertyAnimation { id: layout1buttonOut;running: false; target: layout1button;property: "x";from:110; to: 0;duration: 300; onStarted: isLayoutListDisabled=true}
    PropertyAnimation { id: layout2buttonOut;running: false; target: layout2button;property: "x";from:220; to: 0;duration: 300;
                        onStopped: {isLayoutListOpen=false; isLayoutListDisabled=false}}

    function triggerMenuOutAnimation()
    {
        videosListButtonOut.start()
        videosLayoutButtonOut.start()
        settingsButtonOut.start()

    }
    function triggerVideoButtonsInAnimation()
    {
        video1buttonIn.start()
        video2buttonIn.start()
        video3buttonIn.start()
        video4buttonIn.start()
        video5buttonIn.start()
        webClientButtonIn.start()
    }
    function triggerVideoButtonsOutAnimation()
    {
        video1buttonOut.start()
        video2buttonOut.start()
        video3buttonOut.start()
        video4buttonOut.start()
        video5buttonOut.start()
        webClientButtonOut.start()
    }
    function triggerLayoutButtonsInAnimation()
    {
        layout1buttonIn.start()
        layout2buttonIn.start()
    }
    function triggerLayoutButtonsOutAnimation()
    {
        layout1buttonOut.start()
        layout2buttonOut.start()
    }
}

