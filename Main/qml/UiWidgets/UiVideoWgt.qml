import QtQuick 2.9
import elbit.qml.video 1.0
import QtMultimedia 5.5
import QtQuick.Window 2.3
//import MyProjectQMLEnums 1.0

import ".././UiWidgets" as MyWidgets
import ".././"

Item {
    id:videoId
    visible: true // ONLY FOR DEBUGGING
    property string publisherName:     ""
    property int divAvrageCount: 0
    property int fillMode: UiSystemDefs.crop

    VideoSubscriberItem
    {
        id: video_subscriber
        videoName: publisherName
    }
    VideoOutput {
        id:                     videoOutputId
        source:                 video_subscriber
        width:                  videoId.width
        height:                 videoId.height
        focus :                 visible // to receive focus and capture key events when visible

        fillMode: videoId.fillMode

        Rectangle
        {
            id: no_signal_rect
            anchors.fill: parent
            color: "#FF000000"
            visible: (video_subscriber === null || video_subscriber === "undefined") ?  true : video_subscriber.videoTimeout

            Text
            {
                text: StringStore.string_no_video
                font.pixelSize: 70
                color: UiSystemDefs.whiteColor
                font.family:UiSystemDefs.systemFont
                anchors.centerIn: parent
            }
        }
    }
}
